﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Avantik.Web.Service.Model.Contract;
using Avantik.Web.Service.Infrastructure;
using Avantik.Web.Service.COMHelper;
using entity = Avantik.Web.Service.Entity.Booking;
using System.Data;
using ADODB;
using Avantik.Web.Service.Model.COM.Extension;
using Avantik.Web.Service.Exception.Booking;
using Avantik.Web.Service.Entity.Agency;
using System.Collections;
using System.Data.OleDb;
using Avantik.Web.Service.Helpers;
using Avantik.Web.Service.Entity;
using Avantik.Web.Service.Entity.Booking;

namespace Avantik.Web.Service.Model.COM
{
    public class BookingModelService : RunComplus ,IBookingModelService
    {
        string _server = string.Empty;
        string _user = string.Empty;
        string _pass = string.Empty;
        string _domain = string.Empty;

        bool result = false;
        public BookingModelService(string server, string user, string pass, string domain)
            :base(user,pass,domain)
        {
            _server = server;
            _user = user;
            _pass = pass;
            _domain = domain;
        }
        public bool SaveBooking(entity.Booking booking,
                                bool createTickets,
                                bool readBooking,
                                bool readOnly,
                                bool bSetLock,
                                bool bCheckSeatAssignment,
                                bool bCheckSessionTimeOut)
        {
            tikAeroProcess.Booking objBooking = null;

            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsFlight = null;
            ADODB.Recordset rsQuote = null;

            string strBookingId = string.Empty;
            string strOther = string.Empty;
            string strUserId = string.Empty;
            string strAgencyCode = string.Empty;
            string strCurrency = string.Empty;

            short iAdult = 0;
            short iChild = 0;
            short iInfant = 0;
            short iOther = 0;

            tikAeroProcess.BookingSaveError enumSaveError = tikAeroProcess.BookingSaveError.OK;

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;
                }
                else
                { objBooking = new tikAeroProcess.Booking(); }

                objBooking.GetEmpty(ref rsHeader,
                                    ref rsSegment,
                                    ref rsPassenger,
                                    ref rsRemark,
                                    ref rsPayment,
                                    ref rsMapping,
                                    ref rsService,
                                    ref rsTax,
                                    ref rsFees,
                                    ref rsFlight,
                                    ref rsQuote,
                                    ref strBookingId,
                                    ref strAgencyCode,
                                    ref strCurrency,
                                    ref iAdult,
                                    ref iChild,
                                    ref iInfant,
                                    ref iOther,
                                    ref strOther,
                                    ref strUserId);

                if (booking.Header != null)
                {
                    booking.Header.ToRecordset(ref rsHeader);
                }
                if (booking.Segments != null && booking.Segments.Count > 0)
                {
                    booking.Segments.ToRecordset(ref rsSegment);
                }
                if (booking.Passengers != null && booking.Passengers.Count > 0)
                {
                    booking.Passengers.ToRecordset(ref rsPassenger);
                }
                if (booking.Fees != null && booking.Fees.Count > 0)
                {
                    booking.Fees.ToRecordset(ref rsFees);
                }
                if (booking.Remarks != null && booking.Remarks.Count > 0)
                {
                    booking.Remarks.ToRecordset(ref rsRemark);
                }
                if (booking.Payments != null && booking.Payments.Count > 0)
                {
                    booking.Payments.ToRecordset(ref rsPayment);
                }
                if (booking.Mappings != null && booking.Mappings.Count > 0)
                {
                    booking.Mappings.ToRecordset(ref rsMapping);
                }
                if (booking.Services != null && booking.Services.Count > 0)
                {
                    booking.Services.ToRecordset(ref rsService);
                }
                if (booking.Taxs != null && booking.Taxs.Count > 0)
                {
                    booking.Taxs.ToRecordset(ref rsTax);
                }

                enumSaveError = objBooking.Save(ref rsHeader,
                                                  ref rsSegment,
                                                  ref rsPassenger,
                                                  ref rsRemark,
                                                  ref rsPayment,
                                                  ref rsMapping,
                                                  ref rsService,
                                                  ref rsTax,
                                                  ref rsFees,
                                                  ref createTickets,
                                                  ref readBooking,
                                                  ref readOnly,
                                                  ref bSetLock,
                                                  ref bCheckSeatAssignment,
                                                  ref bCheckSessionTimeOut);

                if (enumSaveError == tikAeroProcess.BookingSaveError.OK)
                {
                    result = true;
                }
                else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGINUSE)
                {
                    throw new BookingSaveException("BOOKINGINUSE");
                }
                else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGREADERROR)
                {
                    throw new BookingSaveException("BOOKINGREADERROR");
                }
                else if (enumSaveError == tikAeroProcess.BookingSaveError.DATABASEACCESS)
                {
                    throw new BookingSaveException("DATABASEACCESS");
                }
                else if (enumSaveError == tikAeroProcess.BookingSaveError.DUPLICATESEAT)
                {
                    throw new BookingSaveException("DUPLICATESEAT","277");
                }
                else if (enumSaveError == tikAeroProcess.BookingSaveError.SESSIONTIMEOUT)
                {
                    throw new BookingSaveException("SESSIONTIMEOUT", "H005");
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
            }

            return result;
        }

        public entity.Booking ReadBooking(string bookingId,
                                   string bookingReference,
                                   double bookingNumber,
                                   bool bReadonly,
                                   bool bSeatLock,
                                   string userId,
                                   bool bReadHeader,
                                   bool bReadSegment,
                                   bool bReadPassenger,
                                   bool bReadRemark,
                                   bool bReadPayment,
                                   bool bReadMapping,
                                   bool bReadService,
                                   bool bReadTax,
                                   bool bReadFee,
                                   bool bReadOd,
                                   string releaseBookingId,
                                   string CompleteRemarkId)

        {
            tikAeroProcess.Booking objBooking = null;

            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsQuote = null;

            entity.Booking booking = new entity.Booking();

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;
                }
                else
                { objBooking = new tikAeroProcess.Booking(); }

                if (!String.IsNullOrEmpty(bookingId))
                {
                    if (bookingId.Equals(Guid.Empty.ToString()))
                        bookingId = string.Empty;

                    if (objBooking.Read("{" + bookingId.ToUpper() + "}",
                                        bookingReference,
                                        bookingNumber,
                                        ref rsHeader,
                                        ref rsSegment,
                                        ref rsPassenger,
                                        ref rsRemark,
                                        ref rsPayment,
                                        ref rsMapping,
                                        ref rsService,
                                        ref rsTax,
                                        ref rsFees,
                                        ref rsQuote,
                                        ref bReadonly,
                                        ref bSeatLock,
                                        ref userId,
                                        bReadHeader,
                                        bReadSegment,
                                        bReadPassenger,
                                        bReadRemark,
                                        bReadPayment,
                                        bReadMapping,
                                        bReadService,
                                        bReadTax,
                                        bReadFee,
                                        bReadOd,
                                        ref releaseBookingId,
                                        ref CompleteRemarkId) == true)

                    {

                        if (rsHeader != null && rsHeader.RecordCount > 0)
                        {
                            booking.Header = booking.Header.FillBooking(rsHeader);
                        }
                        if (rsSegment != null && rsSegment.RecordCount > 0)
                        {
                            booking.Segments = booking.Segments.FillBooking(rsSegment);
                        }
                        if (rsPassenger != null && rsPassenger.RecordCount > 0)
                        {
                            booking.Passengers = booking.Passengers.FillBooking(rsPassenger);
                        }
                        if (rsFees != null && rsFees.RecordCount > 0)
                        {
                            booking.Fees = booking.Fees.FillBooking(rsFees);
                        }
                        if (rsRemark != null && rsRemark.RecordCount > 0)
                        {
                            booking.Remarks = booking.Remarks.FillBooking(rsRemark);
                        }
                        if (rsQuote != null && rsQuote.RecordCount > 0)
                        {
                            booking.Quotes = booking.Quotes.FillBooking(rsQuote);
                        }
                        if (rsTax != null && rsTax.RecordCount > 0)
                        {
                            booking.Taxs = booking.Taxs.FillBooking(rsTax);
                        }
                        if (rsService != null && rsService.RecordCount > 0)
                        {
                            booking.Services = booking.Services.FillBooking(rsService);
                        }
                        if (rsMapping != null && rsMapping.RecordCount > 0)
                        {
                            booking.Mappings = booking.Mappings.FillBooking(rsMapping);
                        }
                        if (rsPayment != null && rsPayment.RecordCount > 0)
                        {
                            booking.Payments = booking.Payments.FillBooking(rsPayment);
                        }
                    }
                }
            }
            catch (BookingException bookingExc)
            {
                throw bookingExc;
            }
            catch
            {
                throw new System.Exception("Read booking is error");
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
                RecordsetHelper.ClearRecordset(ref rsQuote);
            }

            return booking;
        }

        public bool CancelBooking(string bookingId,
                                  string bookingReference,
                                  double bookingNumber,
                                  string userId,
                                  string agencyCode,
                                  bool bWaveFee,
                                  bool bVoid)
        {
            tikAeroProcess.Booking objBooking = null;

            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsQuote = null;
            entity.Booking booking = new entity.Booking();
            bool bCancelBooking = false;
            bool bCancelSegment = false;

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;
                }
                else
                { objBooking = new tikAeroProcess.Booking(); }

                if (!String.IsNullOrEmpty(bookingId) && !String.IsNullOrEmpty(userId))
                {
                    Agent agent = new Agent();
                    AgencyService agencyService = new AgencyService(_server, _user, _pass, _domain);
                    agent = agencyService.GetAgencySessionProfile(agencyCode, userId);

                    if (objBooking.Read("{" + bookingId.ToUpper() + "}",
                                        bookingReference,
                                        bookingNumber,
                                        ref rsHeader,
                                        ref rsSegment,
                                        ref rsPassenger,
                                        ref rsRemark,
                                        ref rsPayment,
                                        ref rsMapping,
                                        ref rsService,
                                        ref rsTax,
                                        ref rsFees,
                                        ref rsQuote,
                                        false,
                                        false,
                                        "{" + userId.ToUpper() + "}",
                                        true,
                                        true,
                                        true,
                                        true,
                                        true,
                                        true,
                                        true,
                                        true,
                                        false,
                                        true,
                                        string.Empty,
                                        string.Empty) == true)
                    {
                        if (rsHeader != null && rsHeader.RecordCount > 0)
                        {
                            booking.Header = booking.Header.FillBooking(rsHeader);
                        }
                        if (rsSegment != null && rsSegment.RecordCount > 0)
                        {
                            booking.Segments = booking.Segments.FillBooking(rsSegment);
                        }
                        if (rsMapping != null && rsMapping.RecordCount > 0)
                        {
                            booking.Mappings = booking.Mappings.FillBooking(rsMapping);
                        }


                        if (!booking.Header.BookingId.Equals(Guid.Empty) && !booking.Header.BookingId.Equals(String.Empty))
                        {
                            if (!booking.IsLockBooking())
                            {
                                bool processTicketRefund = false;
                                bool IncludeRefundableOnly = true;
                                DateTime dtNoShowSegmentDatetime = new DateTime();
                                bool bReturnConfirmCancelStatus = true;
                                Int32 cntBoarded = 0;
                                Int32 cntChecked = 0;
                                Int32 cntFlown = 0;

                                //Get ProcessRefundFlag for Check Agency Refund
                                if (agent.ProcessRefundFlag == 1)
                                    processTicketRefund = true;

                                for (int i = 0; i < booking.Segments.Count; i++)
                                {
                                    if (!booking.IsCheckedPassenger(booking.Segments[i].BookingSegmentId))
                                    {
                                        if (!booking.IsBoardedPassenger(booking.Segments[i].BookingSegmentId))
                                        {
                                            if (!booking.IsFlownPassenger(booking.Segments[i].BookingSegmentId))
                                            {
                                                bCancelSegment = objBooking.SegmentCancel("{" + Convert.ToString(booking.Segments[i].BookingSegmentId).ToUpper() + "}",
                                                                                ref rsSegment,
                                                                                ref rsMapping,
                                                                                ref rsService,
                                                                                ref rsPayment,
                                                                                ref rsTax,
                                                                                ref rsQuote,
                                                                                ref userId,
                                                                                ref agencyCode,
                                                                                ref bWaveFee,
                                                                                ref processTicketRefund,
                                                                                ref IncludeRefundableOnly,
                                                                                ref dtNoShowSegmentDatetime,
                                                                                ref bVoid,
                                                                                ref bReturnConfirmCancelStatus);
                                                if (bCancelSegment != true) return false;
                                            }
                                            else
                                            {
                                                cntFlown++;
                                                if (cntFlown == booking.Segments.Count)
                                                    throw new ModifyBookingException("Passenger check in status as FLOWN", "L005");
                                            }
                                        }
                                        else
                                        {
                                            cntBoarded++;
                                            if (cntBoarded == booking.Segments.Count)
                                                throw new ModifyBookingException("Passenger check in status as BOARDED", "L004");
                                        }
                                    }
                                    else
                                    {
                                        cntChecked++;
                                        if (cntChecked == booking.Segments.Count)
                                            throw new ModifyBookingException("Passenger check in status as CHECKED", "L003");
                                    }
                                }

                                tikAeroProcess.BookingSaveError enumSaveError = tikAeroProcess.BookingSaveError.OK;

                                if (bCancelSegment)
                                {
                                    enumSaveError = objBooking.Save(ref rsHeader,
                                                                     ref rsSegment,
                                                                     ref rsPassenger,
                                                                     ref rsRemark,
                                                                     ref rsPayment,
                                                                     ref rsMapping,
                                                                     ref rsService,
                                                                     ref rsTax,
                                                                     ref rsFees,
                                                                     false,
                                                                     false,
                                                                     false,
                                                                     false,
                                                                     false,
                                                                     false);
                                }
                                if (enumSaveError == tikAeroProcess.BookingSaveError.OK)
                                {
                                    bCancelBooking = true;
                                }
                                else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGINUSE)
                                {
                                    throw new BookingSaveException("BOOKINGINUSE");
                                }
                                else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGREADERROR)
                                {
                                    throw new BookingSaveException("BOOKINGREADERROR");
                                }
                                else if (enumSaveError == tikAeroProcess.BookingSaveError.DATABASEACCESS)
                                {
                                    throw new BookingSaveException("DATABASEACCESS");
                                }
                                else if (enumSaveError == tikAeroProcess.BookingSaveError.DUPLICATESEAT)
                                {
                                    throw new BookingSaveException("DUPLICATESEAT");
                                }
                                else if (enumSaveError == tikAeroProcess.BookingSaveError.SESSIONTIMEOUT)
                                {
                                    throw new BookingSaveException("SESSIONTIMEOUT");
                                }
                            }
                            else
                            {
                                throw new ModifyBookingException("Booking is Locked.", "L001");
                            }
                        }

                    }
                }
            }
            catch (BookingSaveException saveExc)
            {
                throw saveExc;
            }
            catch (ModifyBookingException bookingExc)
            {
                throw bookingExc;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
                RecordsetHelper.ClearRecordset(ref rsQuote);
            }

            return bCancelBooking;
        }

        public bool CancelSegment(string bookingId,
                                  string segmentId,
                                  string userId,
                                  string agencyCode,
                                  bool bWaveFee,
                                  bool bVoid)
        {
            tikAeroProcess.Booking objBooking = null;

            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsQuote = null;
            entity.Booking booking = new entity.Booking();
            bool bCancelSegment = false;
            bool IsSave = false;
            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;
                }
                else
                { objBooking = new tikAeroProcess.Booking(); }

                if (!String.IsNullOrEmpty(bookingId) && !bookingId.Equals(Guid.Empty.ToString()))
                {
                    Agent agent = new Agent();
                    AgencyService agencyService = new AgencyService(_server, _user, _pass, _domain);
                    agent = agencyService.GetAgencySessionProfile(agencyCode, userId);

                    if (agent != null && agent.B2BAllowCancelFlight == 1)
                    {
                        if (objBooking.Read("{" + bookingId.ToUpper() + "}",
                                            string.Empty,
                                            0,
                                            ref rsHeader,
                                            ref rsSegment,
                                            ref rsPassenger,
                                            ref rsRemark,
                                            ref rsPayment,
                                            ref rsMapping,
                                            ref rsService,
                                            ref rsTax,
                                            ref rsFees,
                                            ref rsQuote,
                                            false,
                                            false,
                                            "{" + userId.ToUpper() + "}",
                                            true,
                                            true,
                                            true,
                                            true,
                                            true,
                                            true,
                                            true,
                                            true,
                                            false,
                                            true,
                                            string.Empty,
                                            string.Empty) == true)
                        {
                            if (rsHeader != null && rsHeader.RecordCount > 0)
                            {
                                booking.Header = booking.Header.FillBooking(rsHeader);
                            }
                            if (rsSegment != null && rsSegment.RecordCount > 0)
                            {
                                booking.Segments = booking.Segments.FillBooking(rsSegment);
                            }
                            if (rsMapping != null && rsMapping.RecordCount > 0)
                            {
                                booking.Mappings = booking.Mappings.FillBooking(rsMapping);
                            }
                            if (rsPassenger != null && rsPassenger.RecordCount > 0)
                            {
                                booking.Passengers = booking.Passengers.FillBooking(rsPassenger);
                            }

                            if (!booking.Header.BookingId.Equals(Guid.Empty) && !booking.Header.BookingId.Equals(String.Empty))
                            {
                                if (!booking.IsLockBooking())
                                {
                                    bool processTicketRefund = false;
                                    bool IncludeRefundableOnly = true;
                                    DateTime dtNoShowSegmentDatetime = new DateTime();
                                    bool bReturnConfirmCancelStatus = true;

                                    //Get ProcessRefundFlag for Check Agency Refund
                                    if (agent.ProcessRefundFlag == 1)
                                        processTicketRefund = true;

                                    if (rsSegment != null && rsSegment.RecordCount > 0)
                                    {
                                        Guid flightConnectionId = Guid.Empty;
                                        Guid tmpSegmentId = Guid.Empty;

                                        for (int i = 0; i < booking.Segments.Count; i++)
                                        {
                                            if (!booking.IsCheckedPassenger(booking.Segments[i].BookingSegmentId))
                                            {
                                                if (!booking.IsFlownPassenger(booking.Segments[i].BookingSegmentId))
                                                {
                                                    if (!booking.IsBoardedPassenger(booking.Segments[i].BookingSegmentId))
                                                    {
                                                        if (booking.IsSegmentInBooking(segmentId))
                                                        {
                                                            if (booking.IsValidSegmentStatus(segmentId))
                                                            {
                                                                if (booking.Segments[i].BookingSegmentId == Guid.Parse(segmentId))
                                                                {
                                                                    flightConnectionId = booking.Segments[i].FlightConnectionId;
                                                                    tmpSegmentId = Guid.Parse(segmentId);

                                                                    bCancelSegment = objBooking.SegmentCancel("{" + booking.Segments[i].BookingSegmentId + "}",
                                                                                                    ref rsSegment,
                                                                                                    ref rsMapping,
                                                                                                    ref rsService,
                                                                                                    ref rsPayment,
                                                                                                    ref rsTax,
                                                                                                    ref rsQuote,
                                                                                                    ref userId,
                                                                                                    ref agencyCode,
                                                                                                    ref bWaveFee,
                                                                                                    ref processTicketRefund,
                                                                                                    ref IncludeRefundableOnly,
                                                                                                    ref dtNoShowSegmentDatetime,
                                                                                                    ref bVoid,
                                                                                                    ref bReturnConfirmCancelStatus);


                                                                    if (bCancelSegment)
                                                                    {
                                                                        IsSave = true;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                throw new ModifyBookingException("SegmentId already cancelled.", "302");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            throw new ModifyBookingException("BookingSegmentId is not in Booking.", "F006");
                                                        }

                                                    }
                                                    else
                                                    {
                                                        throw new ModifyBookingException("Passenger check in status as BOARDED", "L004");
                                                    }
                                                }
                                                else
                                                {
                                                    throw new ModifyBookingException("Passenger check in status as FLOWN", "L005");
                                                }
                                            }
                                            else
                                            {
                                                throw new ModifyBookingException("Passenger check in status as CHECKED", "L003");
                                            }
                                        }
                                        // connecting flight  need to review
                                        if (flightConnectionId != Guid.Empty)
                                        {
                                            for (int i = 0; i < booking.Segments.Count; i++)
                                            {
                                                if (booking.Segments[i].FlightConnectionId == flightConnectionId && booking.Segments[i].BookingSegmentId != tmpSegmentId)
                                                {
                                                    bCancelSegment = objBooking.SegmentCancel("{" + booking.Segments[i].BookingSegmentId + "}",
                                                                                    ref rsSegment,
                                                                                    ref rsMapping,
                                                                                    ref rsService,
                                                                                    ref rsPayment,
                                                                                    ref rsTax,
                                                                                    ref rsQuote,
                                                                                    ref userId,
                                                                                    ref agencyCode,
                                                                                    ref bWaveFee,
                                                                                    ref processTicketRefund,
                                                                                    ref IncludeRefundableOnly,
                                                                                    ref dtNoShowSegmentDatetime,
                                                                                    ref bVoid,
                                                                                    ref bReturnConfirmCancelStatus);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    throw new ModifyBookingException("Booking is Lock", "L001");
                                }
                            }

                            tikAeroProcess.BookingSaveError enumSaveError = tikAeroProcess.BookingSaveError.OK;

                            if (IsSave)
                            {
                                enumSaveError = objBooking.Save(ref rsHeader,
                                                                 ref rsSegment,
                                                                 ref rsPassenger,
                                                                 ref rsRemark,
                                                                 ref rsPayment,
                                                                 ref rsMapping,
                                                                 ref rsService,
                                                                 ref rsTax,
                                                                 ref rsFees,
                                                                 false,
                                                                 false,
                                                                 false,
                                                                 false,
                                                                 false,
                                                                 false);
                            }
                            if (enumSaveError == tikAeroProcess.BookingSaveError.OK)
                            {
                                bCancelSegment = true;
                            }
                            else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGINUSE)
                            {
                                throw new BookingSaveException("BOOKINGINUSE");
                            }
                            else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGREADERROR)
                            {
                                throw new BookingSaveException("BOOKINGREADERROR");
                            }
                            else if (enumSaveError == tikAeroProcess.BookingSaveError.DATABASEACCESS)
                            {
                                throw new BookingSaveException("DATABASEACCESS");
                            }
                            else if (enumSaveError == tikAeroProcess.BookingSaveError.DUPLICATESEAT)
                            {
                                throw new BookingSaveException("DUPLICATESEAT");
                            }
                            else if (enumSaveError == tikAeroProcess.BookingSaveError.SESSIONTIMEOUT)
                            {
                                throw new BookingSaveException("SESSIONTIMEOUT");
                            }
                        }
                    }
                    else
                    {
                        throw new ModifyBookingException("Agents not allowed cancel flight", "P014");
                    }
                }
                else
                {
                    throw new ModifyBookingException("Require Booking ID", "B009");
                }
            }
            catch (BookingSaveException saveExc)
            {
                throw saveExc;
            }
            catch (ModifyBookingException bookingExc)
            {
                throw bookingExc;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
                RecordsetHelper.ClearRecordset(ref rsQuote);
            }

            return bCancelSegment;
        }

        public entity.Booking BookFlight(string strAgencyCode,
                                            string strCurrency,
                                            IList<entity.Flight> flight,
                                            string strBookingId,
                                            short iAdult,
                                            short iChild,
                                            short iInfant,
                                            short iOther,
                                            string strOther,
                                            string strUserId,
                                            string strIpAddress,
                                            string strLanguageCode,
                                            bool bNoVat)
        {
            tikAeroProcess.Booking objBooking = null;

            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsQuote = null;
            ADODB.Recordset rsFlights = RecordsetHelper.FabricateFlightRecordset();

            entity.Booking booking = new entity.Booking();

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;
                }
                else
                { objBooking = new tikAeroProcess.Booking(); }

                if (flight != null && flight.Count > 0)
                {
                    flight.ToRecordset(ref rsFlights);
                }

               string result = objBooking.GetEmpty(ref rsHeader,
                                ref rsSegment,
                                ref rsPassenger,
                                ref rsRemark,
                                ref rsPayment,
                                ref rsMapping,
                                ref rsService,
                                ref rsTax,
                                ref rsFees,
                                ref rsFlights,
                                ref rsQuote,
                                ref strBookingId,
                                ref strAgencyCode,
                                ref strCurrency,
                                ref iAdult,
                                ref iChild,
                                ref iInfant,
                                ref iOther,
                                ref strOther,
                                ref strUserId,
                                ref strIpAddress,
                                ref strLanguageCode,
                                ref bNoVat) ;

               if (result != string.Empty)
               {
                   if (rsHeader != null && rsHeader.RecordCount > 0)
                   {
                       booking.Header = booking.Header.FillBooking(rsHeader);
                   }
                   if (rsSegment != null && rsSegment.RecordCount > 0)
                   {
                       booking.Segments = booking.Segments.FillBooking(rsSegment);
                   }
                   if (rsPassenger != null && rsPassenger.RecordCount > 0)
                   {
                       booking.Passengers = booking.Passengers.FillBooking(rsPassenger);
                   }
                   if (rsFees != null && rsFees.RecordCount > 0)
                   {
                       booking.Fees = booking.Fees.FillBooking(rsFees);
                   }
                   if (rsRemark != null && rsRemark.RecordCount > 0)
                   {
                       booking.Remarks = booking.Remarks.FillBooking(rsRemark);
                   }
                   if (rsQuote != null && rsQuote.RecordCount > 0)
                   {
                       booking.Quotes = booking.Quotes.FillBooking(rsQuote);
                   }
                   if (rsTax != null && rsTax.RecordCount > 0)
                   {
                       booking.Taxs = booking.Taxs.FillBooking(rsTax);
                   }
                   if (rsService != null && rsService.RecordCount > 0)
                   {
                       booking.Services = booking.Services.FillBooking(rsService);
                   }
                   if (rsMapping != null && rsMapping.RecordCount > 0)
                   {
                       booking.Mappings = booking.Mappings.FillBooking(rsMapping);
                   }
                   if (rsPayment != null && rsPayment.RecordCount > 0)
                   {
                       booking.Payments = booking.Payments.FillBooking(rsPayment);
                   }
               }
               else
               {
                   throw new BookingException("Add flight fail : " + result);
               }
            }
            catch
            {
                throw ;
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
                RecordsetHelper.ClearRecordset(ref rsQuote);
            }
            return booking;
        }

        public entity.Booking CalculateBookingChange(string strBookingId,
                                            IList<entity.Flight> flight,
                                            string strUserId,
                                            string strAgencyCode,
                                            bool bNoVat)
        {
            tikAeroProcess.Booking objBooking = null;

            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsQuote = null;
            ADODB.Recordset rsFlights = RecordsetHelper.FabricateFlightRecordset();

            entity.Booking booking = new entity.Booking();

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;
                }
                else
                { objBooking = new tikAeroProcess.Booking(); }

                if (!String.IsNullOrEmpty(strBookingId))
                {
                    Boolean resultAdd = false;

                    resultAdd = BookingChangeFlight(strBookingId,
                                                    ref rsHeader,
                                                    ref rsSegment,
                                                    ref rsPassenger,
                                                    ref rsRemark,
                                                    ref rsPayment,
                                                    ref rsMapping,
                                                    ref rsService,
                                                    ref rsTax,
                                                    ref rsFees,
                                                    ref rsQuote,
                                                    flight,
                                                    strUserId,
                                                    strAgencyCode,
                                                    "",
                                                    "",
                                                    bNoVat);
                    if (resultAdd)
                    {
                        if (rsHeader != null && rsHeader.RecordCount > 0)
                        {
                            booking.Header = booking.Header.FillBooking(rsHeader);
                        }
                        if (rsSegment != null && rsSegment.RecordCount > 0)
                        {
                            booking.Segments = booking.Segments.FillBooking(rsSegment);
                        }
                        if (rsPassenger != null && rsPassenger.RecordCount > 0)
                        {
                            booking.Passengers = booking.Passengers.FillBooking(rsPassenger);
                        }
                        if (rsFees != null && rsFees.RecordCount > 0)
                        {
                            booking.Fees = booking.Fees.FillBooking(rsFees);
                        }
                        if (rsRemark != null && rsRemark.RecordCount > 0)
                        {
                            booking.Remarks = booking.Remarks.FillBooking(rsRemark);
                        }
                        if (rsQuote != null && rsQuote.RecordCount > 0)
                        {
                            booking.Quotes = booking.Quotes.FillBooking(rsQuote);
                        }
                        if (rsTax != null && rsTax.RecordCount > 0)
                        {
                            booking.Taxs = booking.Taxs.FillBooking(rsTax);
                        }
                        if (rsService != null && rsService.RecordCount > 0)
                        {
                            booking.Services = booking.Services.FillBooking(rsService);
                        }
                        if (rsMapping != null && rsMapping.RecordCount > 0)
                        {
                            booking.Mappings = booking.Mappings.FillBooking(rsMapping);
                        }
                        if (rsPayment != null && rsPayment.RecordCount > 0)
                        {
                            booking.Payments = booking.Payments.FillBooking(rsPayment);
                        }

                    }else
                    {
                        booking = null;
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
                RecordsetHelper.ClearRecordset(ref rsQuote);
            }
            return booking;
        }
        
        public entity.Booking ChangeFlightFee(string strBookingId,
                                    IList<entity.Flight> flight,
                                    string strUserId,
                                    string agencyCode,string currency,string language,
                                    bool bNoVat)
        {
            tikAeroProcess.Booking objBooking = null;
            tikAeroProcess.clsCreditCard objCreditCard = null;
            tikAeroProcess.Payment objPayment = null;
            tikAeroProcess.Fees objFees = null;

            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsQuote = null;
            ADODB.Recordset rsFlights = RecordsetHelper.FabricateFlightRecordset();

            string strXml = string.Empty;
            Boolean resultAddFlight = false;
            string paymentType = string.Empty;
            entity.Booking bookingResponse = new entity.Booking();
            entity.Booking booking = new entity.Booking();
            var objSegmentIdMapping = new List<KeyValuePair<string, string>>();

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;

                    remote = Type.GetTypeFromProgID("tikAeroProcess.Fees", _server);
                    objFees = (tikAeroProcess.Fees)Activator.CreateInstance(remote);
                    remote = null;

                }
                else
                {
                    objBooking = new tikAeroProcess.Booking();
                    objFees = new tikAeroProcess.Fees();
                }

                if (!String.IsNullOrEmpty(strBookingId))
                {
                    //read booking first for prepare RS 
                    if (objBooking.Read("{" + strBookingId.ToUpper() + "}",
                        string.Empty,
                        0,
                        ref rsHeader,
                        ref rsSegment,
                        ref rsPassenger,
                        ref rsRemark,
                        ref rsPayment,
                        ref rsMapping,
                        ref rsService,
                        ref rsTax,
                        ref rsFees,
                        ref rsQuote,
                        false,
                        false,
                        "{" + strUserId.ToUpper() + "}",
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        string.Empty,
                        string.Empty) == true)
                    {
                        // convert rs to booking for preparing header and mapping  value
                        if (rsMapping != null && rsMapping.RecordCount > 0)
                        {
                            booking.Mappings = booking.Mappings.FillBooking(rsMapping);
                        }
                    }


                    // Found flight need to add flight first  return rs fee,mapping,segment
                    if (flight != null && flight.Count > 0)
                    {
                        // we can get rs from add flight
                        resultAddFlight = BookingChangeFlight(strBookingId,
                                                        ref rsHeader,
                                                        ref rsSegment,
                                                        ref rsPassenger,
                                                        ref rsRemark,
                                                        ref rsPayment,
                                                        ref rsMapping,
                                                        ref rsService,
                                                        ref rsTax,
                                                        ref rsFees,
                                                        ref rsQuote,
                                                        flight,
                                                        strUserId,
                                                        agencyCode,
                                                        currency,
                                                        language,
                                                        bNoVat);
                    }
                        
                    //success add flight return fee,seg,mapping
                    if (resultAddFlight)
                    {
                        // if found US status  return error
                        if (objSegmentIdMapping == null || objSegmentIdMapping.Count == 0)
                        {
                        }

                        if (rsFees != null && rsFees.RecordCount > 0)
                        {
                            booking.Fees = booking.Fees.FillBooking(rsFees);
                        }

                        bookingResponse.Fees = booking.Fees;
                    }
                    else
                    {
                        // add flight fail
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                if (objCreditCard != null)
                {
                    Marshal.FinalReleaseComObject(objCreditCard);
                    objCreditCard = null;
                }
                if (objFees != null)
                {
                    Marshal.FinalReleaseComObject(objFees);
                    objFees = null;
                }
                if (objPayment != null)
                {
                    Marshal.FinalReleaseComObject(objPayment);
                    objPayment = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
                RecordsetHelper.ClearRecordset(ref rsQuote);
            }
            return bookingResponse;
        }


        public entity.Booking  ModifyBooking(string strBookingId,
                                            IList<entity.Flight> flight,
                                            IList<entity.PassengerService> services,
                                            IList<Entity.SeatAssign> seatAssigns,
                                            IList<Entity.Booking.Fee> correctionFee,
                                            IList<Entity.NameChange> NameChange,
                                            IList<entity.Payment> payments,
                                            string strUserId,
                                            string agencyCode,
                                            string currencyRcd,
                                            string languageRcd,
                                            bool bNoVat,
                                            string actionCode)
        {
            tikAeroProcess.Booking objBooking = null;
           // tikAeroProcess.clsCreditCard objCreditCard = null;
            tikAeroProcess.Payment objPayment = null;
            tikAeroProcess.Fees objFees = null;

            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsQuote = null;
            ADODB.Recordset rsFlights = RecordsetHelper.FabricateFlightRecordset();

            string strXml = string.Empty;
            Boolean resultAddFlight = false;
            bool createTickets = false;
            bool readBooking = false;
            bool readOnly = false;
            bool bSetLock = false;
            // can not check dup seat in save booking because we save payment already
            bool bCheckSeatAssignment = false;
            bool bCheckSessionTimeOut = false;
            string paymentType = string.Empty;
            entity.Booking bookingResponse = new entity.Booking();
            entity.Booking booking = new entity.Booking();
            var objSegmentIdMapping = new List<KeyValuePair<string, string>>();
            bool IsProcessSuccess = true;
            bool IsPaymentSuccess = false;
            decimal totalOutStanding = 0;
            string strError = string.Empty;
            tikAeroProcess.BookingSaveError enumSaveError = tikAeroProcess.BookingSaveError.OK;

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;

                    remote = Type.GetTypeFromProgID("tikAeroProcess.Fees", _server);
                    objFees = (tikAeroProcess.Fees)Activator.CreateInstance(remote);
                    remote = null;

                    remote = Type.GetTypeFromProgID("tikAeroProcess.Payment", _server);
                    objPayment = (tikAeroProcess.Payment)Activator.CreateInstance(remote);
                    remote = null;
                }
                else
                {
                    objBooking = new tikAeroProcess.Booking();
                    objFees = new tikAeroProcess.Fees();
                    objPayment = new tikAeroProcess.Payment();
                }

                if (!String.IsNullOrEmpty(strBookingId))
                {
                    //read booking first for prepare RS 
                    if (objBooking.Read("{" + strBookingId.ToUpper() + "}",
                        string.Empty,
                        0,
                        ref rsHeader,
                        ref rsSegment,
                        ref rsPassenger,
                        ref rsRemark,
                        ref rsPayment,
                        ref rsMapping,
                        ref rsService,
                        ref rsTax,
                        ref rsFees,
                        ref rsQuote,
                        false,
                        false,
                        "{" + strUserId.ToUpper() + "}",
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        string.Empty,
                        string.Empty) == true)
                    {
                        // convert rs to booking for preparing header and mapping  value
                        if (rsHeader != null && rsHeader.RecordCount > 0)
                        {
                            booking.Header = booking.Header.FillBooking(rsHeader);
                        }
                        if (rsPassenger != null && rsPassenger.RecordCount > 0)
                        {
                            booking.Passengers = booking.Passengers.FillBooking(rsPassenger);
                        }
                        if (rsMapping != null && rsMapping.RecordCount > 0)
                        {
                            booking.Mappings = booking.Mappings.FillBooking(rsMapping);
                        }
                        if (rsRemark != null && rsRemark.RecordCount > 0)
                        {
                            booking.Remarks = booking.Remarks.FillBooking(rsRemark);
                        }
                        if (rsFees != null && rsFees.RecordCount > 0)
                        {
                            booking.Fees = booking.Fees.FillBooking(rsFees);
                        }

                        // Found flight need to add flight first  return RS of fee,mapping,segment
                        if (flight != null && flight.Count > 0)
                        {
                            // check infant over limit
                            string InfantOverLimitFlag = ConfigHelper.ToString("InfantOverLimitFlag");
                            if (InfantOverLimitFlag.ToUpper() == "TRUE")
                            {
                                int numberOfInfant = 0;
                                // count inf
                                foreach (Entity.Booking.Passenger p in booking.Passengers)
                                {
                                    if (p.PassengerTypeRcd == "INF")
                                    {
                                        numberOfInfant += 1;
                                    }
                                }

                                for (int i = 0; i < flight.Count; i++)
                                {
                                    bool result = InfantOverLimit(numberOfInfant, flight[i].FlightId.ToString(), flight[i].OdOriginRcd, flight[i].DestinationRcd, flight[i].BoardingClassRcd);

                                    if (result)
                                    {
                                        IsProcessSuccess = false;
                                        throw new ModifyBookingException("Add flight fail: Over Infant", "F011");
                                    }
                                }
                            }

                            // get rs from add flight
                            resultAddFlight = BookingChangeFlight(strBookingId,
                                                            ref rsHeader,
                                                            ref rsSegment,
                                                            ref rsPassenger,
                                                            ref rsRemark,
                                                            ref rsPayment,
                                                            ref rsMapping,
                                                            ref rsService,
                                                            ref rsTax,
                                                            ref rsFees,
                                                            ref rsQuote,
                                                            flight,
                                                            strUserId,
                                                            agencyCode,
                                                            currencyRcd,
                                                            languageRcd,
                                                            bNoVat);
                            //success add flight return fee,seg,mapping
                            if (resultAddFlight)
                            {
                                //making segment id mapping : NOT yet suport connection flight
                                if (rsSegment != null && rsSegment.RecordCount > 0)
                                {
                                    booking.Segments = booking.Segments.FillBooking(rsSegment);
                                }

                                // find new segment: key == old seg, Value = new Seg
                                objSegmentIdMapping = FindNewSegmentFlightChange(booking.Segments);

                                // if found US status  return error
                                if (objSegmentIdMapping == null || objSegmentIdMapping.Count == 0)
                                {
                                    IsProcessSuccess = false;
                                    Logger.SaveLog("Modify Booking", DateTime.Now, DateTime.Now, "SegmentIdMapping is error", "");
                                    throw new ModifyBookingException("Add flight fail: Segment status invalid", "F011");
                                }
                                else
                                {
                                    // change flight success  if found seat cancel seat
                                    if (objSegmentIdMapping != null && objSegmentIdMapping.Count > 0)
                                    {
                                        booking.Mappings = CancelSeatInChangeFlight(objSegmentIdMapping, booking.Mappings);
                                    }
                                }
                            }
                            else
                            {
                                IsProcessSuccess = false;
                                // add flight fail

                                throw new ModifyBookingException("Add flight fail", "F011");
                            }
                        }//end add flight

                        //*********************************************************************

                        //Found SSR  set SSR to RS  Add Remark
                        if (IsProcessSuccess && services != null && services.Count > 0)
                        {
                            // if new seg  assign new seg id to ssr
                            if (objSegmentIdMapping != null && objSegmentIdMapping.Count > 0)
                            {
                                services.SetNewSegmentId(objSegmentIdMapping);
                            }
                            // convert ssr to RS
                            services.ToRecordset(ref rsService);

                            // add SSR remark 
                            IList<Remark> remarkList = AddRemark(strBookingId, agencyCode, strUserId);
                            remarkList.ToRecordset(ref rsRemark);

                        }// end ssr
                        //*********************************************************************
                        //Found SEAT and set seat to mapping
                        // return rs fee, mapping
                        if (IsProcessSuccess && seatAssigns != null && seatAssigns.Count > 0)
                        {
                            // set seat to mapping
                            booking.Mappings = SetSeatToMapping(booking.Mappings, seatAssigns, strUserId);

                          //  string xx = XMLHelper.Serialize(booking.Mappings, false);

                            //if change flight merge seat mapping to RS new mapping
                            foreach (Entity.Booking.Mapping m in booking.Mappings)
                            {
                                if (m.PassengerStatusRcd == "OK" && !string.IsNullOrEmpty(m.SeatNumber))
                                {
                                    if (objSegmentIdMapping != null && objSegmentIdMapping.Count > 0)
                                    {
                                        foreach (var element in objSegmentIdMapping)
                                        {
                                            // check seat to new mapping or not
                                            if (m.BookingSegmentId == new Guid(element.Key))
                                            {
                                                // assign seat to new mapping
                                                SetSeatToNewRsMapping(ref rsMapping, objSegmentIdMapping, m, strUserId);
                                            }
                                            else
                                            {
                                                // assign seat to old mapping
                                                SetSeatToSameRsMapping(ref rsMapping, m, strUserId);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // assign seat to old mapping
                                        SetSeatToSameRsMapping(ref rsMapping, m, strUserId);
                                    }
                                }
                            }

                            // ************ void seat fee if found double seat fee************
                            // void seat fee if new select case not paid
                            if (booking.Fees != null && booking.Fees.Count > 0)
                            {
                                VoidSeatFee(ref rsFees, seatAssigns, strUserId);
                            }


                        }// end seat
                        //*********************************************************************

                        //NameChange
                        if (IsProcessSuccess && NameChange != null && NameChange.Count > 0)
                        {
                            // updated name in mapping
                            SetNameToRsMapping(ref rsMapping, NameChange , strUserId);

                            //updated name in passenger
                            SetNameToRsPassenger(ref rsPassenger, NameChange, strUserId);
                        } 
                        // end name change
                        //*********************************************************************

                        //Add fee (bag + ssr + seat + name) to RS
                        if (IsProcessSuccess && correctionFee != null && correctionFee.Count > 0)
                        {

                            // if change flight set new seg id to fee
                            if (objSegmentIdMapping != null && objSegmentIdMapping.Count > 0)
                            {
                                correctionFee = SetNewIdToFee(objSegmentIdMapping, correctionFee);
                            }

                            // set user update
                            correctionFee = AddCreateBy(correctionFee,new Guid(strUserId));

                            // map fee to rs
                            correctionFee.ToRecordset(ref rsFees);

                        }

                        // set pnrupd create by
                        // updated header new userid new time always
                        if (IsProcessSuccess)
                        {
                            UpdateFeeCreateBy(ref rsFees, strUserId);
                            UpdateMappingCreateBy(ref rsMapping, strUserId);
                            UpdateHeader(ref rsHeader, booking.Header, strUserId);
                            UpdateSegmentCreateBy(ref rsSegment, strUserId);
                            UpdateSSRCreateBy(ref rsService, strUserId);
                        }

                        //  when action code PRI : cal balance
                        // replace booking.fee from RSFee again
                        booking.Fees = booking.Fees.FillBooking(rsFees);
                       
                        booking.Mappings = booking.Mappings.FillBooking(rsMapping);

                        totalOutStanding = booking.CalOutStandingBalance(booking.Fees, booking.Mappings);


                        if (actionCode.Trim().ToUpper() == "PRI")
                        {
                            bookingResponse.Fees = bookingResponse.Fees.FillBooking(rsFees);
                            bookingResponse.Mappings = bookingResponse.Mappings.FillBooking(rsMapping);
                            bookingResponse.Services = bookingResponse.Services.FillBooking(rsService);
                        }
                        else if (actionCode.Trim().ToUpper() == "CON")
                        {
                            // save payment
                            if (IsProcessSuccess)
                            {
                                bool IsBookNow = false;

                                // for payment cal balance + allocate
                                booking = RsFillBooking(rsHeader, rsSegment, rsPassenger, rsFees, rsRemark, rsQuote, rsTax, rsService, rsMapping);

                                if (payments != null && payments.Count > 0)
                                {
                                    IsPaymentSuccess = COBPayment(booking, payments, strUserId, agencyCode, currencyRcd, totalOutStanding);
                                }
                                else
                                {
                                    IsBookNow = true;
                                }

                                // save booking
                                if (IsPaymentSuccess || IsBookNow)
                                {
                                    enumSaveError = objBooking.Save(ref rsHeader,
                                                                      ref rsSegment,
                                                                      ref rsPassenger,
                                                                      ref rsRemark,
                                                                      ref rsPayment,
                                                                      ref rsMapping,
                                                                      ref rsService,
                                                                      ref rsTax,
                                                                      ref rsFees,
                                                                      ref createTickets,
                                                                      ref readBooking,
                                                                      ref readOnly,
                                                                      ref bSetLock,
                                                                      ref bCheckSeatAssignment,
                                                                      ref bCheckSessionTimeOut);


                                    if (enumSaveError == tikAeroProcess.BookingSaveError.OK)
                                    {
                                        Boolean resultTicket = false;

                                        // for create ticket
                                        if (IsPaymentSuccess)
                                            resultTicket = objBooking.TicketCreate(agencyCode, strUserId, strBookingId);

                                        if (resultTicket || IsBookNow)
                                        {
                                            //read booking  
                                            if (objBooking.Read("{" + strBookingId.ToUpper() + "}",
                                                string.Empty,
                                                0,
                                                ref rsHeader,
                                                ref rsSegment,
                                                ref rsPassenger,
                                                ref rsRemark,
                                                ref rsPayment,
                                                ref rsMapping,
                                                ref rsService,
                                                ref rsTax,
                                                ref rsFees,
                                                ref rsQuote,
                                                false,
                                                false,
                                                "{" + strUserId.ToUpper() + "}",
                                                true,
                                                true,
                                                true,
                                                true,
                                                true,
                                                true,
                                                true,
                                                true,
                                                true,
                                                true,
                                                string.Empty,
                                                string.Empty) == true)
                                            {
                                                // convert rs to booking for preparing header and mapping  value
                                                if (rsHeader != null && rsHeader.RecordCount > 0)
                                                {
                                                    bookingResponse.Header = booking.Header.FillBooking(rsHeader);
                                                }
                                                if (rsSegment != null && rsSegment.RecordCount > 0)
                                                {
                                                    bookingResponse.Segments = booking.Segments.FillBooking(rsSegment);
                                                }
                                                if (rsPassenger != null && rsPassenger.RecordCount > 0)
                                                {
                                                    bookingResponse.Passengers = booking.Passengers.FillBooking(rsPassenger);
                                                }
                                                if (rsFees != null && rsFees.RecordCount > 0)
                                                {
                                                    bookingResponse.Fees = booking.Fees.FillBooking(rsFees);
                                                }
                                                if (rsRemark != null && rsRemark.RecordCount > 0)
                                                {
                                                    bookingResponse.Remarks = booking.Remarks.FillBooking(rsRemark);
                                                }
                                                if (rsQuote != null && rsQuote.RecordCount > 0)
                                                {
                                                    bookingResponse.Quotes = booking.Quotes.FillBooking(rsQuote);
                                                }
                                                if (rsTax != null && rsTax.RecordCount > 0)
                                                {
                                                    bookingResponse.Taxs = booking.Taxs.FillBooking(rsTax);
                                                }
                                                if (rsService != null && rsService.RecordCount > 0)
                                                {
                                                    bookingResponse.Services = booking.Services.FillBooking(rsService);
                                                }
                                                if (rsMapping != null && rsMapping.RecordCount > 0)
                                                {
                                                    bookingResponse.Mappings = booking.Mappings.FillBooking(rsMapping);
                                                }
                                                if (rsPayment != null && rsPayment.RecordCount > 0)
                                                {
                                                    bookingResponse.Payments = booking.Payments.FillBooking(rsPayment);
                                                }
                                            }
                                            else
                                            {
                                                strError = "Read booking fail";
                                                throw new ModifyBookingException("Read booking fail", "B008");
                                            }
                                        }
                                        else
                                        {
                                            strError = "CREATE TICKET FAIL";

                                            throw new ModifyBookingException("CREATE TICKET FAIL", "P006");
                                        }
                                    }
                                    else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGINUSE)
                                    {
                                        strError = "BOOKINGINUSE";
                                        throw new BookingSaveException("BOOKINGINUSE");
                                    }
                                    else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGREADERROR)
                                    {
                                        strError = "BOOKINGREADERROR";
                                        throw new BookingSaveException("BOOKINGREADERROR");
                                    }
                                    else if (enumSaveError == tikAeroProcess.BookingSaveError.DATABASEACCESS)
                                    {
                                        strError = "DATABASEACCESS";
                                        throw new BookingSaveException("DATABASEACCESS");
                                    }
                                    else if (enumSaveError == tikAeroProcess.BookingSaveError.DUPLICATESEAT)
                                    {
                                        strError = "DUPLICATESEAT";
                                        throw new BookingSaveException("Duplicated Seat", "277");

                                    }
                                    else if (enumSaveError == tikAeroProcess.BookingSaveError.SESSIONTIMEOUT)
                                    {
                                        strError = "SESSIONTIMEOUT";
                                        throw new BookingSaveException("SESSIONTIMEOUT", "H005");
                                    }
                                } // end payment
                                else
                                {
                                    throw new ModifyBookingException("Payment fail", "P001");
                                }
                            }// end process
                            else
                            {
                                throw new ModifyBookingException("Modify booking fail", "300");
                            }
                        }
                        else
                        {
                            // invalid action code
                            strError = "Invalid ActionCode";
                            throw new ModifyBookingException("Invalid ActionCode", "");
                        }
                    }
                    else
                    {
                        throw new ModifyBookingException("Read booking fail", "B008");
                    }
                }
            }
            catch (ModifyBookingException)
            {
                throw;//new ModifyBookingException("Payment fail", "P001");
            }
            catch (BookingSaveException)
            {
                throw;//new ModifyBookingException("Payment fail", "P001");
            }
            catch
            {
                throw;// new ModifyBookingException(strError);
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                if (objFees != null)
                {
                    Marshal.FinalReleaseComObject(objFees);
                    objFees = null;
                }
                if (objPayment != null)
                {
                    Marshal.FinalReleaseComObject(objPayment);
                    objPayment = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
                RecordsetHelper.ClearRecordset(ref rsQuote);

            }
            return bookingResponse;
        }

        private void SetNameToRsPassenger(ref ADODB.Recordset rsPassenger, IList<Entity.NameChange> NameChange, string strUserId)
        {
            foreach (NameChange n in NameChange)
            {
                rsPassenger.MoveFirst();
                while (!rsPassenger.EOF)
                {
                    if (n.PassengerId == RecordsetHelper.ToGuid(rsPassenger, "passenger_id"))
                    {
                        if (!string.IsNullOrEmpty(n.TitleRcd))
                        {
                            rsPassenger.Fields["title_rcd"].Value = n.TitleRcd;
                        }
                        if (!string.IsNullOrEmpty(n.GenderTypeRcd))
                        {
                            rsPassenger.Fields["gender_type_rcd"].Value = n.GenderTypeRcd.ToUpper();
                        }
                        if (!string.IsNullOrEmpty(n.Firstname))
                            rsPassenger.Fields["firstname"].Value = n.Firstname.ToUpper();
                        if (!string.IsNullOrEmpty(n.Middlename))
                            rsPassenger.Fields["middlename"].Value = n.Middlename.ToUpper();
                        if (!string.IsNullOrEmpty(n.Lastname))
                            rsPassenger.Fields["lastname"].Value = n.Lastname.ToUpper();
                        if (n.DateOfBirth != DateTime.MinValue)
                            rsPassenger.Fields["date_of_birth"].Value = n.DateOfBirth;

                        rsPassenger.Fields["update_date_time"].Value = DateTime.Now;
                        rsPassenger.Fields["update_by"].Value = new Guid(strUserId).ToRsString();
                        rsPassenger.Fields["create_date_time"].Value = DateTime.Now;
                        rsPassenger.Fields["create_by"].Value = new Guid(strUserId).ToRsString();
                    }
                    rsPassenger.MoveNext();
                }
            }

        }

        private void SetNameToRsMapping(ref ADODB.Recordset rsMapping, IList<Entity.NameChange> NameChange, string strUserId)
        {
            foreach (NameChange n in NameChange)
            {
                rsMapping.MoveFirst();
                while (!rsMapping.EOF)
                {
                    if (n.PassengerId == RecordsetHelper.ToGuid(rsMapping, "passenger_id"))
                    {
                        if (!string.IsNullOrEmpty(n.TitleRcd))
                        {
                            rsMapping.Fields["title_rcd"].Value = n.TitleRcd.ToUpper();
                        }
                        if (!string.IsNullOrEmpty(n.GenderTypeRcd))
                        {
                            rsMapping.Fields["gender_type_rcd"].Value = n.GenderTypeRcd.ToUpper();
                        }
                        if (!string.IsNullOrEmpty(n.Firstname))
                            rsMapping.Fields["firstname"].Value = n.Firstname.ToUpper();
                        if (!string.IsNullOrEmpty(n.Lastname))
                            rsMapping.Fields["lastname"].Value = n.Lastname.ToUpper();
                        if (n.DateOfBirth != DateTime.MinValue)
                            rsMapping.Fields["date_of_birth"].Value = n.DateOfBirth;

                        rsMapping.Fields["update_date_time"].Value = DateTime.Now;
                        rsMapping.Fields["update_by"].Value = new Guid(strUserId).ToRsString();
                        rsMapping.Fields["create_date_time"].Value = DateTime.Now;
                        rsMapping.Fields["create_by"].Value = new Guid(strUserId).ToRsString();
                    }
                    rsMapping.MoveNext();
                }
            }

        }
        private void SetSeatToSameRsMappingX(ref ADODB.Recordset rsMapping, IList<Entity.Booking.Mapping> Mappings, string strUserId)
        {
            foreach (entity.Mapping m in Mappings)
            {
                rsMapping.MoveFirst();
                while (!rsMapping.EOF)
                {
                    if (RecordsetHelper.ToGuid(rsMapping, "booking_segment_id") == m.BookingSegmentId && RecordsetHelper.ToGuid(rsMapping, "passenger_id") == m.PassengerId && m.SeatNumber.Length > 0)
                    {
                        rsMapping.Fields["seat_column"].Value = m.SeatColumn;
                        rsMapping.Fields["seat_row"].Value = m.SeatRow;
                        rsMapping.Fields["seat_number"].Value = m.SeatNumber;

                        if (m.SeatFeeRcd != null)
                            rsMapping.Fields["seat_fee_rcd"].Value = m.SeatFeeRcd;

                        rsMapping.Fields["update_date_time"].Value = DateTime.Now;
                        rsMapping.Fields["update_by"].Value = new Guid(strUserId).ToRsString();
                        rsMapping.Fields["create_date_time"].Value = DateTime.Now;
                        rsMapping.Fields["create_by"].Value = new Guid(strUserId).ToRsString();

                        break;
                    }
                    rsMapping.MoveNext();
                }
            }

        }

        private void SetSeatToSameRsMapping(ref ADODB.Recordset rsMapping, Entity.Booking.Mapping m, string strUserId)
        {
            rsMapping.MoveFirst();
            while (!rsMapping.EOF)
            {
                if (RecordsetHelper.ToGuid(rsMapping, "booking_segment_id") == m.BookingSegmentId && RecordsetHelper.ToGuid(rsMapping, "passenger_id") == m.PassengerId && m.SeatNumber.Length > 0)
                {
                    rsMapping.Fields["seat_column"].Value = m.SeatColumn;
                    rsMapping.Fields["seat_row"].Value = m.SeatRow;
                    rsMapping.Fields["seat_number"].Value = m.SeatNumber;

                    if (m.SeatFeeRcd != null)
                        rsMapping.Fields["seat_fee_rcd"].Value = m.SeatFeeRcd;

                    rsMapping.Fields["update_date_time"].Value = DateTime.Now;
                    rsMapping.Fields["update_by"].Value = new Guid(strUserId).ToRsString();
                    rsMapping.Fields["create_date_time"].Value = DateTime.Now;
                    rsMapping.Fields["create_by"].Value = new Guid(strUserId).ToRsString();

                    break;
                }
                rsMapping.MoveNext();
            }
        }

        private void SetSeatToNewRsMappingX(ref ADODB.Recordset rsMapping, List<KeyValuePair<string, string>> objSegmentIdMapping, IList<Entity.Booking.Mapping> Mappings, string strUserId)
        {
            foreach (var element in objSegmentIdMapping)
            {
                foreach (entity.Mapping m in Mappings)
                {
                    rsMapping.MoveFirst();
                    while (!rsMapping.EOF)
                    {
                        //old id == mapping_Segid && RSSegId == new id && RS_passId == mapping_PassId
                        if (element.Key.ToUpper().Equals(m.BookingSegmentId.ToString().ToUpper()) && RecordsetHelper.ToGuid(rsMapping, "booking_segment_id") == new Guid(element.Value) && RecordsetHelper.ToGuid(rsMapping, "passenger_id") == m.PassengerId && m.SeatNumber != null && m.SeatNumber.Length > 0)
                        {
                            rsMapping.Fields["seat_column"].Value = m.SeatColumn;
                            rsMapping.Fields["seat_row"].Value = m.SeatRow;
                            rsMapping.Fields["seat_number"].Value = m.SeatNumber;

                            if (m.SeatFeeRcd != null)
                                rsMapping.Fields["seat_fee_rcd"].Value = m.SeatFeeRcd;

                            rsMapping.Fields["update_date_time"].Value = DateTime.Now;
                            rsMapping.Fields["update_by"].Value = new Guid(strUserId).ToRsString();

                            rsMapping.Fields["create_date_time"].Value = DateTime.Now;
                            rsMapping.Fields["create_by"].Value = new Guid(strUserId).ToRsString();

                            break;
                        }

                        rsMapping.MoveNext();
                    }
                }
            }

        }

        private void SetSeatToNewRsMapping(ref ADODB.Recordset rsMapping, List<KeyValuePair<string, string>> objSegmentIdMapping, Entity.Booking.Mapping m, string strUserId)
        {
            foreach (var element in objSegmentIdMapping)
            {
                    rsMapping.MoveFirst();
                    while (!rsMapping.EOF)
                    {
                        //old id == mapping_Segid && RSSegId == new id && RS_passId == mapping_PassId
                        if (element.Key.ToUpper().Equals(m.BookingSegmentId.ToString().ToUpper()) && RecordsetHelper.ToGuid(rsMapping, "booking_segment_id") == new Guid(element.Value) && RecordsetHelper.ToGuid(rsMapping, "passenger_id") == m.PassengerId && m.SeatNumber != null && m.SeatNumber.Length > 0)
                        {
                            rsMapping.Fields["seat_column"].Value = m.SeatColumn;
                            rsMapping.Fields["seat_row"].Value = m.SeatRow;
                            rsMapping.Fields["seat_number"].Value = m.SeatNumber;

                            if (m.SeatFeeRcd != null)
                                rsMapping.Fields["seat_fee_rcd"].Value = m.SeatFeeRcd;

                            rsMapping.Fields["update_date_time"].Value = DateTime.Now;
                            rsMapping.Fields["update_by"].Value = new Guid(strUserId).ToRsString();

                            rsMapping.Fields["create_date_time"].Value = DateTime.Now;
                            rsMapping.Fields["create_by"].Value = new Guid(strUserId).ToRsString();

                            break;
                        }

                        rsMapping.MoveNext();
                    }
            }

        }

        private IList<Entity.Booking.Mapping> SetSeatToMapping(IList<Entity.Booking.Mapping> Mappings, IList<Entity.SeatAssign> seatAssigns, string strUserId)
        {
            for (int i = 0; i < seatAssigns.Count; i++)
            {
                foreach (Entity.Booking.Mapping m in Mappings)
                {
                    if (seatAssigns[i].BookingSegmentID.ToUpper().Equals(m.BookingSegmentId.ToString().ToUpper()) && seatAssigns[i].PassengerID.ToUpper().Equals(m.PassengerId.ToString().ToUpper()))
                    {
                        m.SeatColumn = seatAssigns[i].SeatColumn;
                        m.SeatRow = seatAssigns[i].SeatRow;

                        if (seatAssigns[i].SeatFeeRcd != null)
                            m.SeatFeeRcd = seatAssigns[i].SeatFeeRcd;

                        m.SeatNumber = seatAssigns[i].SeatNumber;

                        m.UpdateBy = new Guid(strUserId);
                        m.UpdateDateTime = DateTime.Now;
                    }
                }
            }

            return Mappings;
        }
       
        private IList<Entity.Booking.Mapping>  CancelSeatInChangeFlight(List<KeyValuePair<string, string>> objSegmentIdMapping, IList<Entity.Booking.Mapping> Mappings)
        {
            foreach (var element in objSegmentIdMapping)
            {
                foreach (entity.Mapping m in Mappings)
                {
                    if (element.Key.ToUpper().Equals(m.BookingSegmentId.ToString().ToUpper()) && m.SeatNumber != null && m.SeatNumber.Length > 0)
                    {
                        m.SeatColumn = string.Empty;
                        m.SeatFeeRcd = string.Empty;
                        m.SeatNumber = string.Empty;
                        m.SeatRow = 0;
                    }
                }
            }

            return Mappings;
        }

        private IList<Entity.Booking.Fee> SetNewIdToFee(List<KeyValuePair<string, string>> objSegmentIdMapping, IList<Entity.Booking.Fee> correctionFee)
        {
            foreach (var element in objSegmentIdMapping)
            {
                foreach (entity.Fee f in correctionFee)
                {
                    if (f.BookingSegmentId == new Guid(element.Key))
                    {
                        f.BookingSegmentId = new Guid(element.Value);
                    }
                }
            }

            return correctionFee;
        }

        private IList<Entity.Booking.Fee> AddCreateBy(IList<Entity.Booking.Fee> correctionFee, Guid user)
        {
            foreach (entity.Fee f in correctionFee)
            {
                f.CreateBy = user;
                f.CreateDateTime = DateTime.Now;
                f.UpdateBy = user;
                f.UpdateDateTime = DateTime.Now;
            }

            return correctionFee;
        }
        
        private void UpdateMappingCreateBy(ref ADODB.Recordset rsMapping, string strUserId)
        {
            if (rsMapping != null && rsMapping.RecordCount > 0)
            {
                rsMapping.MoveFirst();
                while (!rsMapping.EOF)
                {
                    if (RecordsetHelper.ToGuid(rsMapping, "create_by") == Guid.Empty)
                    {
                        rsMapping.Fields["update_date_time"].Value = DateTime.Now;
                        rsMapping.Fields["update_by"].Value = new Guid(strUserId).ToRsString();

                        rsMapping.Fields["create_date_time"].Value = DateTime.Now;
                        rsMapping.Fields["create_by"].Value = new Guid(strUserId).ToRsString();
                    }
                    rsMapping.MoveNext();
                }
            }
        }

        private void VoidSeatFee(ref ADODB.Recordset rsFees, IList<Entity.SeatAssign> seatAssigns, string strUserId)
        {
            foreach (Entity.SeatAssign f in seatAssigns)
            {
                rsFees.MoveFirst();
                while (!rsFees.EOF)
                {
                    if (RecordsetHelper.ToString(rsFees, "fee_category_rcd") == "SEAT"
                        && RecordsetHelper.ToGuid(rsFees, "account_fee_by") == Guid.Empty
                        && RecordsetHelper.ToDateTime(rsFees, "void_date_time") == DateTime.MinValue
                        && new Guid(f.PassengerID) == RecordsetHelper.ToGuid(rsFees, "passenger_id")
                        && new Guid(f.BookingSegmentID) == RecordsetHelper.ToGuid(rsFees, "booking_segment_id")
                        )
                    {
                        rsFees.Fields["void_date_time"].Value = DateTime.Now;
                        rsFees.Fields["void_by"].Value = new Guid(strUserId).ToRsString();
                    }
                    rsFees.MoveNext();
                }
            }

        }

        private void UpdateFeeCreateBy(ref ADODB.Recordset rsFees, string strUserId)
        {
            if (rsFees != null  && rsFees.RecordCount > 0)
            {
                rsFees.MoveFirst();
                while (!rsFees.EOF)
                {
                    if (RecordsetHelper.ToGuid(rsFees, "create_by") == Guid.Empty)
                    {
                        rsFees.Fields["update_date_time"].Value = DateTime.Now;
                        rsFees.Fields["update_by"].Value = new Guid(strUserId).ToRsString();

                        rsFees.Fields["create_date_time"].Value = DateTime.Now;
                        rsFees.Fields["create_by"].Value = new Guid(strUserId).ToRsString();

                       // rsFees.Fields["account_fee_date_time"].Value = DateTime.Now;
                      //  rsFees.Fields["account_fee_by"].Value = new Guid(strUserId).ToRsString();
                    }
                    rsFees.MoveNext();
                }
            }
        }
        private void UpdateSSRCreateBy(ref ADODB.Recordset rsService, string strUserId)
        {
            if (rsService != null && rsService.RecordCount > 0)
            {
                rsService.MoveFirst();
                while (!rsService.EOF)
                {
                    if (RecordsetHelper.ToGuid(rsService, "create_by") == Guid.Empty)
                    {
                        rsService.Fields["update_date_time"].Value = DateTime.Now;
                        rsService.Fields["update_by"].Value = new Guid(strUserId).ToRsString();

                        rsService.Fields["create_date_time"].Value = DateTime.Now;
                        rsService.Fields["create_by"].Value = new Guid(strUserId).ToRsString();

                    }
                    rsService.MoveNext();
                }
            }
        }
        private void UpdateSegmentCreateBy(ref ADODB.Recordset rsSegment, string strUserId)
        {
            if (rsSegment != null && rsSegment.RecordCount > 0)
            {
                rsSegment.MoveFirst();
                while (!rsSegment.EOF)
                {
                    if (RecordsetHelper.ToGuid(rsSegment, "create_by") == Guid.Empty)
                    {
                        rsSegment.Fields["update_date_time"].Value = DateTime.Now;
                        rsSegment.Fields["update_by"].Value = new Guid(strUserId).ToRsString();

                        rsSegment.Fields["create_date_time"].Value = DateTime.Now;
                        rsSegment.Fields["create_by"].Value = new Guid(strUserId).ToRsString();

                    }
                    rsSegment.MoveNext();
                }
            }
        }

        private void UpdateHeader(ref ADODB.Recordset rsHeader, Entity.Booking.BookingHeader Header, string strUserId)
        {
            if (rsHeader != null && rsHeader.RecordCount > 0)
            {
                if (Header.BookingId == RecordsetHelper.ToGuid(rsHeader, "booking_id"))
                {
                    rsHeader.Fields["update_by"].Value = new Guid(strUserId).ToRsString();
                    rsHeader.Fields["update_date_time"].Value = DateTime.Now;
                }
            }
        }

        private IList<Remark> AddRemark(string strBookingId, string agencyCode, string strUserId)
        {
            IList<Remark> remarkList = new List<Remark>();
            Remark remark = new Remark();
            remark.BookingId = new Guid(strBookingId);
            remark.AddedBy = "API / " + agencyCode;
            remark.AgencyCode = agencyCode;
            remark.RemarkText = "Change Reference: API";
            remark.RemarkTypeRcd = "HISTORY";
            remark.UpdateBy = new Guid(strUserId);
            remark.UpdateDateTime = DateTime.Now; ;
            remark.CreateBy = new Guid(strUserId);
            remark.CreateDateTime = DateTime.Now; ;
            remarkList.Add(remark);
            return remarkList;
        }
        
        public bool UpdatePassengerInfo(string strBookingId,
                                            IList<Entity.Booking.Passenger> passenger,
                                            IList<Entity.Booking.Mapping> mapping,
                                            string strUserId,
                                            string agencyCode,
                                            bool bNoVat)
        {
            tikAeroProcess.Booking objBooking = null;
            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsQuote = null;

            string strXml = string.Empty;
            bool createTickets = false;
            bool readBooking = false;
            bool readOnly = false;
            bool bSetLock = false;
            bool bCheckSeatAssignment = false;
            bool bCheckSessionTimeOut = false;
            string paymentType = string.Empty;
            string language = "EN";
            string currency = string.Empty;
            entity.Booking bookingResponse = new entity.Booking();
            entity.Booking booking = new entity.Booking();
            var objSegmentIdMapping = new List<KeyValuePair<string, string>>();
            bool result = false;
            tikAeroProcess.BookingSaveError enumSaveError = tikAeroProcess.BookingSaveError.OK;

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;

                }
                else
                {
                    objBooking = new tikAeroProcess.Booking();
                }

                if (!String.IsNullOrEmpty(strBookingId))
                {
                    //read booking first for prepare RS 
                    if (objBooking.Read("{" + strBookingId.ToUpper() + "}",
                        string.Empty,
                        0,
                        ref rsHeader,
                        ref rsSegment,
                        ref rsPassenger,
                        ref rsRemark,
                        ref rsPayment,
                        ref rsMapping,
                        ref rsService,
                        ref rsTax,
                        ref rsFees,
                        ref rsQuote,
                        false,
                        false,
                        "{" + strUserId.ToUpper() + "}",
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        string.Empty,
                        string.Empty) == true)
                    {
                        // convert rs to booking for preparing header and mapping  value
                        if (rsHeader != null && rsHeader.RecordCount > 0)
                        {
                            booking.Header = booking.Header.FillBooking(rsHeader);
                            currency = booking.Header.CurrencyRcd;
                            language = booking.Header.LanguageRcd;
                        }
                        if (rsPassenger != null && rsPassenger.RecordCount > 0)
                        {
                            booking.Passengers = booking.Passengers.FillBooking(rsPassenger);
                        }
                        if (rsMapping != null && rsMapping.RecordCount > 0)
                        {
                            booking.Mappings = booking.Mappings.FillBooking(rsMapping);
                        }
                    }

                    foreach (entity.Passenger reqPass in passenger)
                    {
                        foreach (entity.Passenger p in booking.Passengers)
                        {
                            if (reqPass.PassengerId == p.PassengerId)
                            {
                                p.DocumentTypeRcd = reqPass.DocumentTypeRcd;
                                p.DocumentNumber = reqPass.DocumentNumber;
                                p.PassportNumber = reqPass.PassportNumber;
                                p.PassportIssueDate = reqPass.PassportIssueDate;
                                p.PassportExpiryDate = reqPass.PassportExpiryDate;
                                p.PassportIssuePlace = reqPass.PassportIssuePlace;
                                p.PassportIssueCountryRcd = reqPass.PassportIssueCountryRcd;
                                p.NationalityRcd = reqPass.NationalityRcd;
                                p.PassportBirthPlace = reqPass.PassportBirthPlace;

                            }
                        }
                    }

                    foreach (entity.Passenger p in booking.Passengers)
                    {
                        rsPassenger.MoveFirst();
                        while (!rsPassenger.EOF)
                        {
                            if (p.PassengerId == RecordsetHelper.ToGuid(rsPassenger, "passenger_id"))
                            {
                                rsPassenger.Fields["document_type_rcd"].Value = p.DocumentTypeRcd;
                                rsPassenger.Fields["passport_birth_place"].Value = p.PassportBirthPlace;
                                rsPassenger.Fields["passport_number"].Value = p.PassportNumber;
                                rsPassenger.Fields["passport_issue_date"].Value = p.PassportIssueDate;
                                rsPassenger.Fields["passport_expiry_date"].Value = p.PassportExpiryDate;
                                rsPassenger.Fields["passport_issue_place"].Value = p.PassportIssuePlace;
                                rsPassenger.Fields["passport_issue_country_rcd"].Value = p.PassportIssueCountryRcd;
                                rsPassenger.Fields["nationality_rcd"].Value = p.NationalityRcd;
                                rsPassenger.Fields["update_by"].Value = new Guid(strUserId).ToRsString();
                                break;
                            }
                            rsPassenger.MoveNext();
                        }
                    }


                    // save booking
                    enumSaveError = objBooking.Save(ref rsHeader,
                                                        ref rsSegment,
                                                        ref rsPassenger,
                                                        ref rsRemark,
                                                        ref rsPayment,
                                                        ref rsMapping,
                                                        ref rsService,
                                                        ref rsTax,
                                                        ref rsFees,
                                                        ref createTickets,
                                                        ref readBooking,
                                                        ref readOnly,
                                                        ref bSetLock,
                                                        ref bCheckSeatAssignment,
                                                        ref bCheckSessionTimeOut);


                    if (enumSaveError == tikAeroProcess.BookingSaveError.OK)
                    {
                        result = true;
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGINUSE)
                    {
                        throw new BookingSaveException("BOOKINGINUSE");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGREADERROR)
                    {
                        throw new BookingSaveException("BOOKINGREADERROR");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.DATABASEACCESS)
                    {
                        throw new BookingSaveException("DATABASEACCESS");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.DUPLICATESEAT)
                    {
                        throw new BookingSaveException("DUPLICATESEAT", "277");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.SESSIONTIMEOUT)
                    {
                        throw new BookingSaveException("SESSIONTIMEOUT", "H005");
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
                RecordsetHelper.ClearRecordset(ref rsQuote);
            }
            return result;
        }

        public bool UpdateContactDetail(string strBookingId,
                                    Entity.Booking.BookingHeader header,
                                    string strUserId,
                                    string agencyCode,
                                    bool bNoVat)
        {
            tikAeroProcess.Booking objBooking = null;
            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsQuote = null;

            string strXml = string.Empty;
            bool createTickets = false;
            bool readBooking = false;
            bool readOnly = false;
            bool bSetLock = false;
            bool bCheckSeatAssignment = false;
            bool bCheckSessionTimeOut = false;
            string paymentType = string.Empty;
            string language = "EN";
            string currency = string.Empty;
            entity.Booking bookingResponse = new entity.Booking();
            entity.Booking booking = new entity.Booking();
            var objSegmentIdMapping = new List<KeyValuePair<string, string>>();
            bool result = false;

            tikAeroProcess.BookingSaveError enumSaveError = tikAeroProcess.BookingSaveError.OK;

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;

                }
                else
                {
                    objBooking = new tikAeroProcess.Booking();
                }

                if (!String.IsNullOrEmpty(strBookingId))
                {
                    //read booking first for prepare RS 
                    if (objBooking.Read("{" + strBookingId.ToUpper() + "}",
                        string.Empty,
                        0,
                        ref rsHeader,
                        ref rsSegment,
                        ref rsPassenger,
                        ref rsRemark,
                        ref rsPayment,
                        ref rsMapping,
                        ref rsService,
                        ref rsTax,
                        ref rsFees,
                        ref rsQuote,
                        false,
                        false,
                        "{" + strUserId.ToUpper() + "}",
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        string.Empty,
                        string.Empty) == true)
                    {
                        // convert rs to booking for preparing header and mapping  value
                        if (rsHeader != null && rsHeader.RecordCount > 0)
                        {
                            booking.Header = booking.Header.FillBooking(rsHeader);
                            currency = booking.Header.CurrencyRcd;
                            language = booking.Header.LanguageRcd;
                        }
                    }


                        if (header.BookingId == RecordsetHelper.ToGuid(rsHeader, "booking_id"))
                        {
                            rsHeader.Fields["contact_name"].Value = header.ContactName;
                            rsHeader.Fields["contact_email"].Value = header.ContactEmail;
                            rsHeader.Fields["phone_mobile"].Value = header.PhoneMobile;

                            rsHeader.Fields["phone_home"].Value = header.PhoneHome;
                            rsHeader.Fields["phone_business"].Value = header.PhoneBusiness;
                            rsHeader.Fields["phone_fax"].Value = header.PhoneFax;
                            rsHeader.Fields["mobile_email"].Value = header.MobileEmail;

                            rsHeader.Fields["language_rcd"].Value = header.LanguageRcd;
                            rsHeader.Fields["update_by"].Value = new Guid(strUserId).ToRsString();
                            
                        }


                    // save booking
                    enumSaveError = objBooking.Save(ref rsHeader,
                                                        ref rsSegment,
                                                        ref rsPassenger,
                                                        ref rsRemark,
                                                        ref rsPayment,
                                                        ref rsMapping,
                                                        ref rsService,
                                                        ref rsTax,
                                                        ref rsFees,
                                                        ref createTickets,
                                                        ref readBooking,
                                                        ref readOnly,
                                                        ref bSetLock,
                                                        ref bCheckSeatAssignment,
                                                        ref bCheckSessionTimeOut);


                    if (enumSaveError == tikAeroProcess.BookingSaveError.OK)
                    {
                        result = true;
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGINUSE)
                    {
                        throw new BookingSaveException("BOOKINGINUSE");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGREADERROR)
                    {
                        throw new BookingSaveException("BOOKINGREADERROR");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.DATABASEACCESS)
                    {
                        throw new BookingSaveException("DATABASEACCESS");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.DUPLICATESEAT)
                    {
                        throw new BookingSaveException("DUPLICATESEAT", "277");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.SESSIONTIMEOUT)
                    {
                        throw new BookingSaveException("SESSIONTIMEOUT", "H005");
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
                RecordsetHelper.ClearRecordset(ref rsQuote);
            }
            return result;
        }



        public bool UpdateTicket(string strBookingId,
                                    IList<Entity.Booking.Mapping> mapping,
                                    string strUserId,
                                    string agencyCode,
                                    bool bNoVat)
        {
            tikAeroProcess.Booking objBooking = null;
            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsQuote = null;

            string strXml = string.Empty;
            bool createTickets = false;
            bool readBooking = false;
            bool readOnly = false;
            bool bSetLock = false;
            bool bCheckSeatAssignment = false;
            bool bCheckSessionTimeOut = false;
            string paymentType = string.Empty;
            string language = "EN";
            string currency = string.Empty;
            entity.Booking bookingResponse = new entity.Booking();
            entity.Booking booking = new entity.Booking();
            var objSegmentIdMapping = new List<KeyValuePair<string, string>>();

            tikAeroProcess.BookingSaveError enumSaveError = tikAeroProcess.BookingSaveError.OK;

            bool result = false;

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;

                }
                else
                {
                    objBooking = new tikAeroProcess.Booking();
                }

                if (!String.IsNullOrEmpty(strBookingId))
                {

                    //read booking first for prepare RS 
                    if (objBooking.Read("{" + strBookingId.ToUpper() + "}",
                        string.Empty,
                        0,
                        ref rsHeader,
                        ref rsSegment,
                        ref rsPassenger,
                        ref rsRemark,
                        ref rsPayment,
                        ref rsMapping,
                        ref rsService,
                        ref rsTax,
                        ref rsFees,
                        ref rsQuote,
                        false,
                        false,
                        "{" + strUserId.ToUpper() + "}",
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        string.Empty,
                        string.Empty) == true)
                    {
                        // convert rs to booking for preparing header and mapping  value
                        if (rsHeader != null && rsHeader.RecordCount > 0)
                        {
                            booking.Header = booking.Header.FillBooking(rsHeader);
                            currency = booking.Header.CurrencyRcd;
                            language = booking.Header.LanguageRcd;
                        }
                        if (rsMapping != null && rsMapping.RecordCount > 0)
                        {
                            booking.Mappings = booking.Mappings.FillBooking(rsMapping);
                        }
                    }


                    foreach (entity.Mapping mm in mapping)
                    {
                        foreach (entity.Mapping m in booking.Mappings)
                        {
                            if (mm.BookingSegmentId == m.BookingSegmentId && mm.PassengerId == m.PassengerId)
                            {
                                m.BaggageWeight = mm.BaggageWeight;
                                m.PieceAllowance = mm.PieceAllowance;
                                m.EndorsementText = mm.EndorsementText;
                                m.RestrictionText = mm.RestrictionText;
                            }
                        }
                    }


                    foreach (entity.Mapping m in booking.Mappings)
                    {
                        rsMapping.MoveFirst();
                        while (!rsMapping.EOF)
                        {
                            if (m.PassengerId == RecordsetHelper.ToGuid(rsMapping, "passenger_id")
                                &&
                                m.BookingSegmentId == RecordsetHelper.ToGuid(rsMapping, "booking_segment_id")
                                )
                            {
                                rsMapping.Fields["endorsement_text"].Value = m.EndorsementText;
                                rsMapping.Fields["restriction_text"].Value = m.RestrictionText;
                                rsMapping.Fields["baggage_weight"].Value = m.BaggageWeight;
                                rsMapping.Fields["piece_allowance"].Value = m.PieceAllowance;
                                rsMapping.Fields["update_by"].Value = new Guid(strUserId).ToRsString();
                                break;
                            }
                            rsMapping.MoveNext();
                        }
                    }


                    // save booking
                    enumSaveError = objBooking.Save(ref rsHeader,
                                                        ref rsSegment,
                                                        ref rsPassenger,
                                                        ref rsRemark,
                                                        ref rsPayment,
                                                        ref rsMapping,
                                                        ref rsService,
                                                        ref rsTax,
                                                        ref rsFees,
                                                        ref createTickets,
                                                        ref readBooking,
                                                        ref readOnly,
                                                        ref bSetLock,
                                                        ref bCheckSeatAssignment,
                                                        ref bCheckSessionTimeOut);


                    if (enumSaveError == tikAeroProcess.BookingSaveError.OK)
                    {
                        result = true;
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGINUSE)
                    {
                        throw new BookingSaveException("BOOKINGINUSE");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.BOOKINGREADERROR)
                    {
                        throw new BookingSaveException("BOOKINGREADERROR");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.DATABASEACCESS)
                    {
                        throw new BookingSaveException("DATABASEACCESS");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.DUPLICATESEAT)
                    {
                        throw new BookingSaveException("DUPLICATESEAT", "277");
                    }
                    else if (enumSaveError == tikAeroProcess.BookingSaveError.SESSIONTIMEOUT)
                    {
                        throw new BookingSaveException("SESSIONTIMEOUT", "H005");
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
                RecordsetHelper.ClearRecordset(ref rsQuote);
            }
            return result;
        }


        public entity.Booking GetQuoteSummary(IList<entity.FlightSegment> flights, IList<entity.Passenger> passengers, string agencyCode, string language, string currencyCode, bool bNovat)
        {
            tikAeroProcess.Fares objFare = null;
            ADODB.Recordset rsFlightSegment = null;
            ADODB.Recordset rsPassengers = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsQuotes = null;
            ADODB.Recordset rsFee = null;

            string strFlightId = string.Empty;
            string strBoardpoint = string.Empty;
            string strOffpoint = string.Empty;
            string strAirline = string.Empty;
            string strFlight = string.Empty;
            string strBoardingClass = string.Empty;
            string strBookingClass = string.Empty;
            DateTime dtFlight = Convert.ToDateTime("12/30/1899");
            string strFareId = string.Empty;
            bool bRefundable = false;
            bool bGroupBooking = false;
            bool bNonRevenue = false;
            string strSegmentId = string.Empty;
            short iIdReduction = 0;
            string strFareType = "FARE";
  
            entity.Booking booking = new entity.Booking();

            try
            {
                if (string.IsNullOrEmpty(_server) == false)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Fees", _server);
                    objFare = (tikAeroProcess.Fares)Activator.CreateInstance(remote);
                    remote = null;
                }
                else
                { objFare = new tikAeroProcess.Fares(); }

                if (flights != null && flights.Count > 0)
                {
                    flights.ToRecordset(ref rsFlightSegment);
                    passengers.ToRecordset(ref rsPassengers);

                    if (objFare.FareQuote(ref rsPassengers,
                                           agencyCode,
                                           ref rsTax,
                                           ref rsMapping,
                                           ref rsQuotes,
                                           ref rsFlightSegment,
                                           ref strFlightId,
                                           ref strBoardpoint,
                                           ref strOffpoint,
                                           ref strAirline,
                                           ref strFlight,
                                           ref strBoardingClass,
                                           ref strBookingClass,
                                           ref dtFlight,
                                           ref strFareId,
                                           ref currencyCode,
                                           ref bRefundable,
                                           ref bGroupBooking,
                                           ref bNonRevenue,
                                           ref strSegmentId,
                                           ref iIdReduction,
                                           ref strFareType,
                                           ref language,
                                           ref bNovat) == true)
                    {

                        if (rsFlightSegment != null && rsFlightSegment.RecordCount > 0)
                            booking.Segments = booking.Segments.FillBooking(rsFlightSegment);
                        if (rsPassengers != null && rsPassengers.RecordCount > 0)
                            booking.Passengers = booking.Passengers.FillBooking(rsPassengers);
                        if (rsTax != null && rsTax.RecordCount > 0)
                            booking.Taxs = booking.Taxs.FillBooking(rsTax);
                        if (rsMapping != null && rsMapping.RecordCount > 0)
                            booking.Mappings = booking.Mappings.FillBooking(rsMapping);
                        if (rsQuotes != null && rsQuotes.RecordCount > 0)
                            booking.Quotes = booking.Quotes.FillBooking(rsQuotes);
                        if (rsFee != null && rsFee.RecordCount > 0)
                            booking.Fees = booking.Fees.FillBooking(rsFee);
                    }
                }

            }
            catch (BookingException bookingExc)
            {
                throw bookingExc;
            }
            finally
            {
                if (objFare != null)
                {
                    Marshal.FinalReleaseComObject(objFare);
                    objFare = null;
                }
                RecordsetHelper.ClearRecordset(ref rsMapping, false);
                RecordsetHelper.ClearRecordset(ref rsFee);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsQuotes);
                RecordsetHelper.ClearRecordset(ref rsFee);
            }
            return booking;
        }

        private bool BookingChangeFlight(string strBookingId,
                                            ref Recordset rsHeader,
                                            ref Recordset rsSegment,
                                            ref Recordset rsPassenger,
                                            ref Recordset rsRemark,
                                            ref Recordset rsPayment,
                                            ref Recordset rsMapping,
                                            ref Recordset rsService,
                                            ref Recordset rsTax,
                                            ref Recordset rsFees,
                                            ref Recordset rsQuote,
                                            IList<entity.Flight> flights,
                                            string strUserId,
                                            string agencyCode,
                                            string  currency,
                                            string  language,
                                            bool bNoVat)
        {
            tikAeroProcess.Booking objBooking = null;

            ADODB.Recordset rsFlights = RecordsetHelper.FabricateFlightRecordset();

            entity.Booking booking = new entity.Booking();

           // Boolean resultChange = false;
            Boolean resultAdd = false;

            try
            {
                {
                    if (rsSegment != null && rsSegment.RecordCount > 0)
                    {
                        booking.Segments = booking.Segments.FillBooking(rsSegment);
                    }
                    if (rsMapping != null && rsMapping.RecordCount > 0)
                    {
                        booking.Mappings = booking.Mappings.FillBooking(rsMapping);
                    }

                    if (flights != null && flights.Count > 0)
                    {
                        List<entity.Flight> flightsSort = flights.OrderByDescending(f => f.DepartureDate).ToList();

                        for (int i = 0; i < flightsSort.Count; i++)
                        {
                            rsSegment.Filter = "segment_status_rcd = 'HK' AND od_origin_rcd = '" + flightsSort[i].OdOriginRcd + "' AND od_destination_rcd = '" + flightsSort[i].OdDestinationRcd + "'";

                            if (rsSegment.RecordCount > 0)
                            {
                                rsSegment.Sort = "departure_date DESC";
                                rsSegment.MoveFirst();

                                while (!rsSegment.EOF)
                                {
                                    Guid curValue = RecordsetHelper.ToGuid(rsSegment, "booking_segment_id");
                                    String flightCheckInStatus = RecordsetHelper.ToString(rsSegment, "flight_check_in_status_rcd");

                                    if (booking.IsCheckedPassenger(curValue))
                                    {
                                        throw new ModifyBookingException("Passenger check in status as CHECKED", "L003");
                                    }
                                    if (booking.IsBoardedPassenger(curValue))
                                    {
                                        throw new ModifyBookingException("Passenger check in status as BOARDED", "L004");
                                    }
                                    if (booking.IsFlownPassenger(curValue))
                                    {
                                        throw new ModifyBookingException("Passenger check in status as FLOWN", "L005");
                                    }
                                    if (flightCheckInStatus.Equals("CLOSED"))
                                    {
                                        throw new ModifyBookingException("Flight segment closed", "L002");
                                    }

                                    if (flightsSort.Count(fgt => fgt.ExchangedSegmentId.ToString() == "" + curValue.ToString() + "") == 0)
                                    {
                                        flightsSort[i].ExchangedSegmentId = curValue;
                                        break;
                                    }

                                    rsSegment.MoveNext();
                                }
                            }
                            else
                            {
                                throw new ModifyBookingException("Route not found", "P009");
                            }
                        }

                        flightsSort.ToRecordset(ref rsFlights);
                    }

                    // Call AddChangeFlight for Add new FlightSegment
                    resultAdd = AddChangeFlight(ref rsHeader,
                                            ref rsPassenger,
                                            ref rsSegment,
                                            ref rsMapping,
                                            ref rsPayment,
                                            ref rsTax,
                                            ref rsFees,
                                            ref rsService,
                                            ref rsQuote,
                                            ref rsRemark,
                                            ref rsFlights,
                                            agencyCode,
                                            strBookingId,
                                            language,
                                            currency,
                                            strUserId,
                                            bNoVat);

                    if (resultAdd)
                    {
                        booking.Fees = AddChangeFees(agencyCode, currency, strBookingId, ref rsHeader, ref rsSegment, ref rsPassenger, ref rsFees, ref rsRemark, ref rsMapping, ref rsService, language, bNoVat);
                    }
                    else
                    {
                        throw new ModifyBookingException("CAN NOT ADD FLIGHT", "P008");
                    }

                }
            }
            catch (ModifyBookingException bookingExc)
            {
                throw bookingExc;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
            }
            return resultAdd;
        }

        private Boolean AddChangeFlight(ref Recordset rsHeader,
                                        ref Recordset rsPassenger, 
                                        ref Recordset rsSegment, 
                                        ref Recordset rsMapping,
                                        ref Recordset rsPayment,
                                        ref Recordset rsTax,
                                        ref Recordset rsFees,
                                        ref Recordset rsService, 
                                        ref Recordset rsQuote, 
                                        ref Recordset rsRemark, 
                                        ref Recordset rsFlights,
                                        String agencyCode,
                                        String strBookingId, 
                                        String language, 
                                        String currency, 
                                        String strUserId,
                                        Boolean bNoVat)
        {
            Boolean result = false;
            tikAeroProcess.Booking objBooking = null;

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;
                }
                else
                {
                    objBooking = new tikAeroProcess.Booking();
                }

                DateTime dtFlight = DateTime.Parse("1899/12/30");
                Boolean bGroupBooking = false;
                Boolean bWaitlist = false;

                result = objBooking.AddFlight(ref rsPassenger,
                                              ref rsSegment,
                                              ref rsMapping,
                                              ref rsTax,
                                              ref rsService,
                                              agencyCode,
                                              ref rsQuote,
                                              ref rsRemark,
                                              ref rsFlights,
                                              dtFlight,
                                              strBookingId,
                                              "", "", "", "", "", "", "", "",
                                              language,
                                              currency,
                                              false,
                                              false,
                                              bGroupBooking,
                                              bWaitlist,
                                              false,
                                              false,
                                              false,
                                              "", "", "",
                                              0,
                                              strUserId,
                                              false,
                                              false,
                                              "",
                                              bNoVat);

                if (!result)
                    throw new BookingException("Add Change Flight : " + result);

            }
            catch
            {
                throw;
            }

            return result;
        }

        
        
        private IList<Entity.Booking.Fee> AddChangeFees(String agencyCode, 
                                   String currency, 
                                   String strBookingId, 
                                   ref Recordset rsHeader, 
                                   ref Recordset rsSegment, 
                                   ref Recordset rsPassenger, 
                                   ref Recordset rsFees, 
                                   ref Recordset rsRemark, 
                                   ref Recordset rsMapping, 
                                   ref Recordset rsService, 
                                   String language, 
                                   Boolean bNoVat)
        {
            Boolean result = false;
            tikAeroProcess.Fees objFees = null;
            Entity.Booking.Booking booking = new Entity.Booking.Booking();

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Fees", _server);
                    objFees = (tikAeroProcess.Fees)Activator.CreateInstance(remote);
                    remote = null;
                }
                else
                {
                    objFees = new tikAeroProcess.Fees();
                }

                result = objFees.BookingChange(agencyCode, currency, strBookingId, ref rsHeader, ref rsSegment, ref rsPassenger, ref rsFees, ref rsRemark, ref rsMapping, rsService, language, bNoVat);

                if (rsFees != null && rsFees.RecordCount > 0)
                {
                    booking.Fees = booking.Fees.FillBooking(rsFees);
                }

            }
            catch
            {
                throw;
            }
            return booking.Fees;
        }


        //
        public bool COBPayment(Booking booking,
                               IList<entity.Payment> payments,
                               string strUserId, string agencyCode, string currencyRcd, decimal totalOutStanding)
        {
            tikAeroProcess.Booking objBooking = null;
            tikAeroProcess.clsCreditCard objCreditCard = null;
            tikAeroProcess.Payment objPayment = null;
            tikAeroProcess.Fees objFees = null;

            ADODB.Recordset rsHeader = null;
            ADODB.Recordset rsSegment = null;
            ADODB.Recordset rsPassenger = null;
            ADODB.Recordset rsRemark = null;
            ADODB.Recordset rsPayment = null;
            ADODB.Recordset rsMapping = null;
            ADODB.Recordset rsService = null;
            ADODB.Recordset rsTax = null;
            ADODB.Recordset rsFees = null;
            ADODB.Recordset rsQuote = null;
            ADODB.Recordset rsAllocation = null;
            string paymentType = string.Empty;
            string strXml = string.Empty;
            string strBookingId = booking.Header.BookingId.ToString();
            bool bNoVat = Convert.ToBoolean(booking.Header.NoVatFlag == 0 ? false : true);
            bool IsPaymentSave = false;

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Booking", _server);
                    objBooking = (tikAeroProcess.Booking)Activator.CreateInstance(remote);
                    remote = null;

                    remote = Type.GetTypeFromProgID("tikAeroProcess.clsCreditCard", _server);
                    objCreditCard = (tikAeroProcess.clsCreditCard)Activator.CreateInstance(remote);
                    remote = null;

                    remote = Type.GetTypeFromProgID("tikAeroProcess.Payment", _server);
                    objPayment = (tikAeroProcess.Payment)Activator.CreateInstance(remote);
                    remote = null;

                }
                else
                {
                    objBooking = new tikAeroProcess.Booking();
                    objCreditCard = new tikAeroProcess.clsCreditCard();
                    objFees = new tikAeroProcess.Fees();
                    objPayment = new tikAeroProcess.Payment();
                }

                    //prepare obj booking

                    if (totalOutStanding > 0)
                    {
                        // get empty rs object
                        rsPayment = objPayment.GetEmpty();

                        if (payments != null && payments.Count == 1) // Fix =1 cause phase 1 suport only single payment
                        {
                            payments[0].DocumentAmount = totalOutStanding;
                            payments[0].PaymentAmount = totalOutStanding;
                            payments[0].ReceivePaymentAmount = totalOutStanding;
                            payments[0].BookingId = new Guid(strBookingId);
                            payments[0].RecordLocator = booking.Header.RecordLocator;

                            if (string.IsNullOrEmpty(payments[0].CurrencyRcd))
                                payments[0].CurrencyRcd = currencyRcd;

                            if (string.IsNullOrEmpty(payments[0].ReceiveCurrencyRcd))
                                payments[0].ReceiveCurrencyRcd = currencyRcd;

                            // agency from booking  to GWS
                            payments[0].AgencyCode = agencyCode;
                            payments[0].CreateBy = new Guid(strUserId);
                            payments[0].UpdateBy = new Guid(strUserId);

                            payments.ToRecordset(ref rsPayment);
                            booking.Payments = booking.Payments.FillBooking(rsPayment);
                            paymentType = booking.Payments[0].FormOfPaymentRcd;
                            rsAllocation = RecordsetHelper.FabricatePaymentAllocationRecordset();
                            IList<Entity.PaymentAllocation> paymentAllocations = null;
                            paymentAllocations = booking.CreateAllocation();
                            PaymentService ps = new PaymentService(_server, _user, _pass, _domain);

                            if (paymentAllocations != null)
                            {
                                //Check Payment Type
                                if (paymentType.Trim().ToUpper().Equals("VOUCHER"))
                                {
                                    IsPaymentSave = ps.PaymentVoucher(totalOutStanding, booking.Mappings, booking.Fees, payments, paymentAllocations, agencyCode,strUserId);
                                }
                                else if (paymentType.Trim().ToUpper().Equals("CRAGT") || paymentType.Trim().ToUpper().Equals("INV"))
                                {
                                    IsPaymentSave = ps.PaymentAgencyAccount(totalOutStanding,paymentType, agencyCode, strUserId, payments, paymentAllocations);
                                }
                                else if (paymentType.Trim().ToUpper().Equals("CC"))
                                {
                                    IsPaymentSave = ps.PaymentCreditCard(booking.Header, payments, paymentAllocations);
                                }
                            }
                        }
                        else
                        {
                            if (payments == null || payments.Count == 0)
                                throw new ModifyBookingException("Required payment information", "P015");
                            else if (payments.Count > 1)
                                throw new ModifyBookingException("Do not support multiple payment", "P016");
                        }
                    }
                    else if (totalOutStanding == 0)
                    {
                        IsPaymentSave = true;
                    }
                    else if (totalOutStanding < 0)
                    {
                        throw new ModifyBookingException("Outstanding balance less than 0", "P017");
                    }
            }
            catch
            {
                throw new ModifyBookingException("Payment fail", "P001"); ;
               // throw;
            }
            finally
            {
                if (objBooking != null)
                {
                    Marshal.FinalReleaseComObject(objBooking);
                    objBooking = null;
                }
                if (objCreditCard != null)
                {
                    Marshal.FinalReleaseComObject(objCreditCard);
                    objCreditCard = null;
                }
                if (objFees != null)
                {
                    Marshal.FinalReleaseComObject(objFees);
                    objFees = null;
                }
                if (objPayment != null)
                {
                    Marshal.FinalReleaseComObject(objPayment);
                    objPayment = null;
                }
                RecordsetHelper.ClearRecordset(ref rsHeader);
                RecordsetHelper.ClearRecordset(ref rsSegment);
                RecordsetHelper.ClearRecordset(ref rsPassenger);
                RecordsetHelper.ClearRecordset(ref rsRemark);
                RecordsetHelper.ClearRecordset(ref rsPayment);
                RecordsetHelper.ClearRecordset(ref rsMapping);
                RecordsetHelper.ClearRecordset(ref rsService);
                RecordsetHelper.ClearRecordset(ref rsTax);
                RecordsetHelper.ClearRecordset(ref rsFees);
                RecordsetHelper.ClearRecordset(ref rsQuote);
            }
            return IsPaymentSave;
        }

        // segment id mapping NOT suport connection flight, Oct 2015
        public List<KeyValuePair<string, string>> FindNewSegmentFlightChange(IList<Entity.Booking.FlightSegment> fs)
        {
            var listSegmentIdMapping = new List<KeyValuePair<string, string>>();
            var listOldSegId = new List<KeyValuePair<string, string>>();
            var listNewSegId = new List<KeyValuePair<string, string>>();

            if (fs != null && fs.Count > 0)
            {
                for (int i = 0; i < fs.Count; i++)
                {
                    if (fs[i].SegmentStatusRcd.ToUpper().Equals("NN"))
                    {
                        listNewSegId.Add(new KeyValuePair<string, string>(fs[i].BookingSegmentId.ToString(), fs[i].OdOriginRcd + fs[i].OdDestinationRcd));
                    }
                    else if (fs[i].SegmentStatusRcd.ToUpper().Equals("XK"))
                    {
                        listOldSegId.Add(new KeyValuePair<string, string>(fs[i].BookingSegmentId.ToString(), fs[i].OdOriginRcd + fs[i].OdDestinationRcd));
                    }

                    // if found status US return null
                    else if (fs[i].SegmentStatusRcd.ToUpper().Equals("US"))
                    {
                        listSegmentIdMapping = null;
                        return listSegmentIdMapping;
                    }
                }

                //for (int i = 0; i < fs.Count; i++)
                //{
                //    if (fs[i].SegmentStatusRcd.ToUpper().Equals("XK"))
                //    {
                //        listOldSegId.Add(new KeyValuePair<string, string>(fs[i].BookingSegmentId.ToString(), fs[i].OdOriginRcd + fs[i].OdDestinationRcd));
                //    }
                //}

                // if found status US return null
                //for (int i = 0; i < fs.Count; i++)
                //{
                //    if (fs[i].SegmentStatusRcd.ToUpper().Equals("US"))
                //    {
                //        listSegmentIdMapping = null;
                //        return listSegmentIdMapping;
                //    }
                //}

                foreach (var elementNew in listNewSegId)
                {
                    foreach (var elementOld in listOldSegId)
                    {
                        if (elementNew.Value.Equals(elementOld.Value))
                        {
                            listSegmentIdMapping.Add(new KeyValuePair<string, string>(elementOld.Key, elementNew.Key));
                        }
                    }
                }

            }

            return listSegmentIdMapping;
        }
        
        public bool SetRSnewSegmentId2(List<KeyValuePair<string, string>> objSegmentIdMapping, ref Recordset rs)
        {
            bool result = false;
            if (objSegmentIdMapping != null && objSegmentIdMapping.Count > 0)
            {
                foreach (var element in objSegmentIdMapping)
                {
                    if (rs != null && rs.RecordCount > 0)
                    {
                        rs.MoveFirst();
                        while (!rs.EOF)
                        {
                            if (RecordsetHelper.ToGuid(rs, "booking_segment_id") == new Guid(element.Key))
                            {
                                rs.Fields["booking_segment_id"].Value = "{" + element.Value + "}";
                                result = true;
                            }
                            rs.MoveNext();
                        }
                    }
                }
            }
            return result;
        }

        private bool PaymentVoucher(Booking booking,
                            IList<Entity.Booking.Payment> payments,
                            IList<Entity.PaymentAllocation> paymentAllocations,
                            decimal totalOutStanding)
        {
            tikAeroProcess.Payment objPayment = null;
            ADODB.Recordset rsPayment = null;

            bool result = false;
            ADODB.Recordset rsAllocation = null;
            ADODB.Recordset rsRefundVoucher = null;

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Payment", _server);
                    objPayment = (tikAeroProcess.Payment)Activator.CreateInstance(remote);
                    remote = null;
                }
                else
                {
                    objPayment = new tikAeroProcess.Payment();
                }

                // get empty rs object
                rsPayment = objPayment.GetEmpty();

                payments.ToRecordset(ref rsPayment);
                booking.Payments = booking.Payments.FillBooking(rsPayment);

                rsAllocation = RecordsetHelper.FabricatePaymentAllocationRecordset();

                Entity.Voucher voucher = new Entity.Voucher();
                VoucherService vcService = new VoucherService(_server, _user, _pass, _domain);
                bool IsValidVoucher = true;

                if (payments != null && payments.Count > 0)
                {
                    foreach (Entity.Booking.Payment p in payments)
                    {
                        voucher.VoucherNumber = p.DocumentNumber;
                        voucher.VoucherPassword = p.DocumentPassword;
                        voucher.CurrencyRcd = p.CurrencyRcd;
                      //  voucher = vcService.GetVoucher(voucher,null,null);

                        if (voucher == null)
                        {
                            IsValidVoucher = false;
                            throw new ModifyBookingException("INVALID VOUCHER", "P011");
                        }
                        else
                        {
                            rsPayment.MoveFirst();
                            while (!rsPayment.EOF)
                            {
                                rsPayment.Fields["voucher_payment_id"].Value = Guid.Parse(voucher.VoucherId).ToRsString();
                                rsPayment.MoveNext();
                            }
                        }
                    }
                }

                //
                if (IsValidVoucher)
                {
                    paymentAllocations.ToRecordset(ref rsAllocation);

                    if (voucher.ValidateVoucherEnough(totalOutStanding) == true)
                    {
                        result = objPayment.Save(ref rsPayment, ref rsAllocation, ref rsRefundVoucher);

                        if (result)
                        {
                            // createTickets = true;
                        }
                        else
                        {
                            throw new ModifyBookingException("PAYMENT FAIL", "P001");
                        }
                    }
                    else
                    {
                        throw new ModifyBookingException("VALUE NOT ENOUGH", "P002");
                    }
                }
                else
                {
                    throw new ModifyBookingException("PAYMENT DOCUMENT NOT FOUND", "P003");
                }
            }
            catch
            {
                throw new ModifyBookingException("PAYMENT FAIL", "P001");
            }
            finally
            {
                if (objPayment != null)
                {
                    Marshal.FinalReleaseComObject(objPayment);
                    objPayment = null;
                }

                RecordsetHelper.ClearRecordset(ref rsPayment);
            }

            return result;
        }


        private bool InfantOverLimit(int numberOfInfant, string flightId, string originRcd, string destinationRcd, string boardingClass)
        {
            tikAeroProcess.Inventory objInventory = null;
            ADODB.Recordset rsInfantLimit = null;
            bool result = false;

            try
            {
                if (_server.Length > 0)
                {
                    Type remote = Type.GetTypeFromProgID("tikAeroProcess.Inventory", _server);
                    objInventory = (tikAeroProcess.Inventory)Activator.CreateInstance(remote);
                    remote = null;

                }
                else
                {
                    objInventory = new tikAeroProcess.Inventory();
                }

                rsInfantLimit = objInventory.GetFlightLegInfants(ref flightId,
                                                                    ref originRcd,
                                                                    ref destinationRcd,
                                                                    ref boardingClass);


                if (rsInfantLimit != null && rsInfantLimit.RecordCount > 0)
                {
                    // if (rsInfantLimit.Fields["infant_capacity"].Value != System.DBNull.Value)
                    {
                        if (Convert.ToInt32(rsInfantLimit.Fields["infant_capacity"].Value) > 0 && numberOfInfant > 0)
                        {
                            if (Convert.ToInt32(rsInfantLimit.Fields["available_infant"].Value) < numberOfInfant)
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }

                        }
                    }
                }

            }
            catch
            {

            }
            finally
            {
                if (objInventory != null)
                {
                    Marshal.FinalReleaseComObject(objInventory);
                    objInventory = null;
                }
                RecordsetHelper.ClearRecordset(ref rsInfantLimit);

            }
            return result;
        }

        private Entity.Booking.Booking RsFillBooking(ADODB.Recordset rsHeader,
                                                ADODB.Recordset rsSegment,
                                                ADODB.Recordset rsPassenger,
                                                ADODB.Recordset rsFees,
                                                ADODB.Recordset rsRemark,
                                                ADODB.Recordset rsQuote,
                                                ADODB.Recordset rsTax,
                                                ADODB.Recordset rsService,
                                                ADODB.Recordset rsMapping)
        {
            Entity.Booking.Booking booking = new Booking();

            if (rsHeader != null && rsHeader.RecordCount > 0)
            {
                booking.Header = booking.Header.FillBooking(rsHeader);
            }
            if (rsSegment != null && rsSegment.RecordCount > 0)
            {
                booking.Segments = booking.Segments.FillBooking(rsSegment);
            }
            if (rsPassenger != null && rsPassenger.RecordCount > 0)
            {
                booking.Passengers = booking.Passengers.FillBooking(rsPassenger);
            }
            if (rsFees != null && rsFees.RecordCount > 0)
            {
                booking.Fees = booking.Fees.FillBooking(rsFees);
            }
            if (rsRemark != null && rsRemark.RecordCount > 0)
            {
                booking.Remarks = booking.Remarks.FillBooking(rsRemark);
            }
            if (rsQuote != null && rsQuote.RecordCount > 0)
            {
                booking.Quotes = booking.Quotes.FillBooking(rsQuote);
            }
            if (rsTax != null && rsTax.RecordCount > 0)
            {
                booking.Taxs = booking.Taxs.FillBooking(rsTax);
            }
            if (rsService != null && rsService.RecordCount > 0)
            {
                booking.Services = booking.Services.FillBooking(rsService);
            }
            if (rsMapping != null && rsMapping.RecordCount > 0)
            {
                booking.Mappings = booking.Mappings.FillBooking(rsMapping);
            }

            return booking;
        }

    }
}
